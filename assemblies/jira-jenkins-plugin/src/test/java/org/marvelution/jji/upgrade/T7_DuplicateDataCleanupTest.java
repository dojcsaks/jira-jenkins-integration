/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Testcase for {@link T7_DuplicateDataCleanup}
 *
 * @author Mark Rekveld
 * @since 2.3.0
 */
public class T7_DuplicateDataCleanupTest extends AbstractUpgradeTaskTest<T7_DuplicateDataCleanup> {

	@Mock
	private SiteService siteService;
	@Mock
	private JobService jobService;
	@Mock
	private BuildService buildService;

	T7_DuplicateDataCleanup createTask() {
		return new T7_DuplicateDataCleanup(stateService, siteService, jobService, buildService);
	}

	@Test
	public void testDoUpgrade() throws Exception {
		Site site = new Site().setId("1").setName("Test Instance");
		when(siteService.getAll()).thenReturn(singletonList(site));
		List<Job> jobs = asList(
				new Job().setId("1").setName("JobA").setSite(site),
				new Job().setId("2").setName("JobB").setSite(site),
				new Job().setId("3").setName("JobB").setSite(site),
				new Job().setId("4").setName("FolderA").setSite(site),
				new Job().setId("5").setName("FolderJobA").setSite(site).setUrlName("FolderA/job/FolderJobA"),
				new Job().setId("6").setName("FolderJobA").setSite(site).setUrlName("FolderA/job/FolderJobA"),
				new Job().setId("7").setName("FolderJobB").setSite(site).setUrlName("FolderA/job/FolderJobB")
		);
		when(jobService.getAllBySite(site, true)).thenReturn(jobs);

		Job job = new Job().setId("1").setName("Job");
		when(jobService.getAll(true)).thenReturn(singletonList(job));
		Set<Build> builds = Stream.of(
				new Build().setId("1").setJob(job).setNumber(1),
				new Build().setId("2").setJob(job).setNumber(2),
				new Build().setId("3").setJob(job).setNumber(2),
				new Build().setId("4").setJob(job).setNumber(3),
				new Build().setId("5").setJob(job).setNumber(4),
				new Build().setId("6").setJob(job).setNumber(4)
		).collect(toSet());
		when(buildService.getByJob(job)).thenReturn(builds);

		assertThat(task.doUpgrade(), nullValue());

		verify(siteService).getAll();
		verify(jobService).getAllBySite(site, true);
		ArgumentCaptor<Job> jobArgumentCaptor = ArgumentCaptor.forClass(Job.class);
		verify(jobService, times(2)).delete(jobArgumentCaptor.capture());
		List<Job> deletedJobs = jobArgumentCaptor.getAllValues();
		assertThat(deletedJobs, hasSize(2));
		assertThat(deletedJobs.stream().filter(j -> j.getName().equals("JobB")).count(), is(1L));
		assertThat(deletedJobs.stream()
		                      .filter(j -> j.getUrlName().equals("FolderA/job/FolderJobA") && j.getName().equals("FolderJobA")).count(),
		           is(1L));

		verify(jobService).getAll(true);
		verify(buildService).getByJob(job);
		ArgumentCaptor<Build> buildArgumentCaptor = ArgumentCaptor.forClass(Build.class);
		verify(buildService, times(2)).delete(buildArgumentCaptor.capture());
		List<Build> deletedBuilds = buildArgumentCaptor.getAllValues();
		assertThat(deletedBuilds, hasSize(2));
		assertThat(deletedBuilds.stream().filter(b -> b.getNumber() == 2).count(), is(1L));
		assertThat(deletedBuilds.stream().filter(b -> b.getNumber() == 4).count(), is(1L));

		verifyNoMoreInteractions(siteService, jobService, buildService);
		verifyZeroInteractions(stateService);
	}
}
