/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.model.Site;

import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import static org.marvelution.jji.model.Matchers.equalTo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link T10_RegenerateSharedSecrets}.
 *
 * @author Mark Rekveld
 * @since 3.1.0
 */
public class T10_RegenerateSharedSecretsTest extends AbstractUpgradeTaskTest<T10_RegenerateSharedSecrets> {

	@Mock
	private SiteService siteService;

	@Override
	T10_RegenerateSharedSecrets createTask() {
		return new T10_RegenerateSharedSecrets(stateService, siteService);
	}

	@Override
	public void testDoUpgrade() throws Exception {
		List<Site> sites = Arrays.asList(
				new Site().setId("site-1")
				          .setName("Site 1")
				          .setRpcUrl(URI.create("http://localhost:8080"))
				          .setJenkinsPluginInstalled(true)
				          .setSharedSecret("old-shared-secret-1")
				          .setUseCrumbs(false),
				new Site().setId("site-2")
				          .setName("Site 2")
				          .setRpcUrl(URI.create("http://localhost:8082"))
				          .setJenkinsPluginInstalled(false)
				          .setSharedSecret("old-shared-secret-2")
				          .setUseCrumbs(true)
		);
		when(siteService.getAll()).thenReturn(sites);

		task.doUpgrade();

		verify(siteService).getAll();
		ArgumentCaptor<Site> argumentCaptor = ArgumentCaptor.forClass(Site.class);
		verify(siteService, times(2)).saveAndSynchronize(argumentCaptor.capture());
		assertThat(argumentCaptor.getAllValues(), hasSize(2));
		assertThat(argumentCaptor.getAllValues(), hasItem(equalTo(new Site().setId("site-1")
		                                                                    .setName("Site 1")
		                                                                    .setRpcUrl(URI.create("http://localhost:8080"))
		                                                                    .setJenkinsPluginInstalled(false)
		                                                                    .setUseCrumbs(true))));
		assertThat(argumentCaptor.getAllValues(), hasItem(equalTo(new Site().setId("site-2")
		                                                                    .setName("Site 2")
		                                                                    .setRpcUrl(URI.create("http://localhost:8082"))
		                                                                    .setJenkinsPluginInstalled(false)
		                                                                    .setUseCrumbs(true))));
	}
}
