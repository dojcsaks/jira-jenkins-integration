/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;

import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import static com.google.common.collect.ImmutableSet.of;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link T8_BuildDisplayNameCleanup}.
 *
 * @author Mark Rekveld
 * @since 2.3.3
 */
public class T8_BuildDisplayNameCleanupTest extends AbstractUpgradeTaskTest<T8_BuildDisplayNameCleanup> {

	@Mock
	private JobService jobService;
	@Mock
	private BuildService buildService;

	@Override
	T8_BuildDisplayNameCleanup createTask() {
		return new T8_BuildDisplayNameCleanup(stateService, jobService, buildService);
	}

	@Override
	public void testDoUpgrade() throws Exception {
		Job job = new Job().setId("1").setName("jira-jenkins-integration");
		when(jobService.getAll(true)).thenReturn(singletonList(job));
		Build noDisplayName = new Build().setJob(job).setNumber(1);
		Build nonDefaultDisplayName = new Build().setJob(job).setNumber(2)
		                                             .setDisplayName("My Custom Build Display Name");
		Build defaultDisplayName = new Build().setJob(job).setNumber(3)
		                                      .setDisplayName(job.getName() + " #3");
		when(buildService.getByJob(job)).thenReturn(of(noDisplayName, nonDefaultDisplayName, defaultDisplayName));

		task.doUpgrade();

		verify(jobService).getAll(true);
		verify(buildService).getByJob(job);
		ArgumentCaptor<Build> buildArgumentCaptor = ArgumentCaptor.forClass(Build.class);
		verify(buildService).save(buildArgumentCaptor.capture());

		Build updatedBuild = buildArgumentCaptor.getValue();
		assertThat(updatedBuild.getJob(), is(job));
		assertThat(updatedBuild.getNumber(), is(3));
		assertThat(updatedBuild.getDisplayName(), is(nullValue()));
	}
}
