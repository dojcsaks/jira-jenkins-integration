/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.ArrayList;
import java.util.List;

import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.model.Site;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Testcase for {@link T5_RemoteSiteStatus}
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class T5_RemoteSiteStatusTest extends AbstractUpgradeTaskTest<T5_RemoteSiteStatus> {

	@Mock
	private SiteService siteService;
	@Mock
	private SiteClient siteClient;

	@Override
	T5_RemoteSiteStatus createTask() {
		return new T5_RemoteSiteStatus(stateService, siteService, siteClient);
	}

	@Test
	public void testDoUpgrade() throws Exception {
		when(siteClient.isCrumbSecurityEnabled(any(Site.class))).thenReturn(true).thenReturn(false);
		when(siteClient.isJenkinsPluginInstalled(any(Site.class))).thenReturn(true).thenReturn(false);
		List<Site> sites = new ArrayList<>(2);
		Site siteA = new Site().setId("1");
		siteA.setName("Site A");
		sites.add(siteA);
		Site siteB = new Site().setId("2");
		siteB.setName("Site B");
		sites.add(siteB);
		when(siteService.getAll()).thenReturn(sites);
		task.doUpgrade();
		verify(siteService).getAll();
		verify(siteClient).isCrumbSecurityEnabled(siteA);
		verify(siteClient).isJenkinsPluginInstalled(siteA);
		verify(siteClient).isCrumbSecurityEnabled(siteB);
		verify(siteClient).isJenkinsPluginInstalled(siteB);
		ArgumentCaptor<Site> argumentCaptor = ArgumentCaptor.forClass(Site.class);
		verify(siteService, times(2)).save(argumentCaptor.capture());
		Site value = argumentCaptor.getAllValues().get(0);
		assertThat(value.getId(), is("1"));
		assertThat(value.isUseCrumbs(), is(true));
		assertThat(value.isJenkinsPluginInstalled(), is(true));
		value = argumentCaptor.getAllValues().get(1);
		assertThat(value.getId(), is("2"));
		assertThat(value.isUseCrumbs(), is(false));
		assertThat(value.isJenkinsPluginInstalled(), is(false));
	}

}
