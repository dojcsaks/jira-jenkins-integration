# SSL TrustStore

The `cacerts` file is a SSL Trust Store that allows testing using the Jenkins installation at <https://ci.jenkins.io>.

It was created using the [SSL for JIRA](https://marketplace.atlassian.com/plugins/com.atlassian.jira.plugin.jirasslplugin/server/overview)
plugin to resolve the [PKIX Path Building Failed SSL Exception](https://confluence.atlassian.com/kb/unable-to-connect-to-ssl-services-due-to-pkix-path-building-failed-779355358.html)
