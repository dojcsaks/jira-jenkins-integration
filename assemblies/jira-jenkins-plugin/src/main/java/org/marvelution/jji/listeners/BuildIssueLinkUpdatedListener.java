/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.listeners;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.events.BuildIssueLinksUpdatedEvent;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;

/**
 * {@link EventListener} to reindex issues on build link updates
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
@Named
public class BuildIssueLinkUpdatedListener extends AbstractEventListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(BuildIssueLinkUpdatedListener.class);
	private final IssueManager issueManager;
	private final IssueIndexingService issueIndexingService;

	@Inject
	public BuildIssueLinkUpdatedListener(@ComponentImport EventPublisher eventPublisher, @ComponentImport IssueManager issueManager,
	                                     @ComponentImport IssueIndexingService issueIndexingService) {
		super(eventPublisher);
		this.issueManager = issueManager;
		this.issueIndexingService = issueIndexingService;
	}

	@EventListener
	public void onBuildIssueLinkUpdate(BuildIssueLinksUpdatedEvent event) {
		try {
			issueIndexingService.reIndexIssueObjects(event.getIssueKeys().stream().map(issueManager::getIssueObject).collect(toSet()),
			                                         IssueIndexingParams.INDEX_ISSUE_ONLY, true);
		} catch (IndexException e) {
			LOGGER.warn("Unable to update index of issues {}", event.getIssueKeys().stream().collect(joining(", ")), e);
		}
	}

}
