/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.Collection;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.StateService;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;

import static org.marvelution.jji.data.services.DefaultConfigurationService.SETTING_PREFIX;
import static org.marvelution.jji.data.services.DefaultConfigurationService.getPluginSettings;

/**
 * {@link PluginUpgradeTask} to cleanup unneeded settings from {@link PluginSettings}
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
@Named
@ExportAsService(PluginUpgradeTask.class)
public class T6_EnabledProjectsSettingsCleanup extends AbstractUpgradeTask {

	private static final String ENABLED_PROJECT_KEYS = SETTING_PREFIX + "enabled_project_keys";
	private final PluginSettingsFactory pluginSettingsFactory;

	@Inject
	public T6_EnabledProjectsSettingsCleanup(StateService stateService, @ComponentImport PluginSettingsFactory pluginSettingsFactory) {
		super(stateService);
		this.pluginSettingsFactory = pluginSettingsFactory;
	}

	@Override
	public Collection<Message> doUpgrade() throws Exception {
		PluginSettings settings = getPluginSettings(pluginSettingsFactory);
		// Since JIRA Software only software projects are enabled
		settings.remove(ENABLED_PROJECT_KEYS);
		return null;
	}
}
