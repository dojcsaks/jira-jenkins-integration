/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.Collection;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.api.StateService;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.groupingBy;

/**
 * @author Mark Rekveld
 * @since 2.3.0
 */
@Named
@ExportAsService(PluginUpgradeTask.class)
public class T7_DuplicateDataCleanup extends AbstractUpgradeTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(T7_DuplicateDataCleanup.class);
	private final SiteService siteService;
	private final JobService jobService;
	private final BuildService buildService;

	@Inject
	public T7_DuplicateDataCleanup(StateService stateService, SiteService siteService, JobService jobService, BuildService buildService) {
		super(stateService);
		this.siteService = siteService;
		this.jobService = jobService;
		this.buildService = buildService;
	}

	@Override
	public Collection<Message> doUpgrade() throws Exception {
		siteService.getAll().forEach(site -> {
			LOGGER.info("Checking for duplicate Jobs on {}", site.getName());
			jobService.getAllBySite(site, true).stream()
			          .collect(groupingBy(Job::getUrlName))
			          .forEach((urlName, jobs) -> {
				          if (jobs.size() > 1) {
					          LOGGER.info("Deleting {} duplicates of job {}.", jobs.size() - 1, urlName);
					          jobs.subList(1, jobs.size()).forEach(jobService::delete);
				          }
			          });
		});
		jobService.getAll(true)
		          .forEach(job -> {
			          LOGGER.info("Checking for duplicate builds of {}", job.getName());
			          buildService.getByJob(job).stream()
			                      .collect(groupingBy(Build::getNumber))
			                      .forEach((number, builds) -> {
				                      if (builds.size() > 1) {
					                      LOGGER.info("Deleting {} duplicates of build {} #{}.", builds.size() - 1, job.getName(), number);
					                      builds.subList(1, builds.size()).forEach(buildService::delete);
				                      }
			                      });
		          });
		return null;
	}
}
