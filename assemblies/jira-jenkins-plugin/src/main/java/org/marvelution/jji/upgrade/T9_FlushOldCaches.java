/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.Collection;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.StateService;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.ManagedCache;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link PluginUpgradeTask} to flush old unused caches.
 *
 * @author Mark Rekveld
 * @since 2.3.5
 */
@Named
@ExportAsService(PluginUpgradeTask.class)
public class T9_FlushOldCaches extends AbstractUpgradeTask {

	static final String[] UNUSED_CACHE_NAMES = new String[] {
			"org.marvelution.jira.plugins.jenkins.sync.impl.DefaultSynchronizer.progressMap",
			"org.marvelution.jira.plugins.jenkins.sync.impl.DefaultSynchronizer.syncProgress"
	};
	private static final Logger LOGGER = LoggerFactory.getLogger(T9_FlushOldCaches.class);
	private final CacheManager cacheManager;

	@Inject
	public T9_FlushOldCaches(StateService stateService, @ComponentImport CacheManager cacheManager) {
		super(stateService);
		this.cacheManager = cacheManager;
	}

	@Override
	public Collection<Message> doUpgrade() throws Exception {
		for (String cacheName : UNUSED_CACHE_NAMES) {
			ManagedCache managedCache = cacheManager.getManagedCache(cacheName);
			if (managedCache != null) {
				LOGGER.info("Clearing unused cache {}.", cacheName);
				managedCache.clear();
			} else {
				LOGGER.debug("Nothing to clear, cache {} doesn't exist anymore.", cacheName);
			}
		}
		return null;
	}
}
