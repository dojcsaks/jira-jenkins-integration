/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest.filter;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.ext.Provider;

import org.marvelution.jji.data.services.api.SiteService;

import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

/**
 * {@link Provider} for the {@link ResourceFilterFactory} and adds the JJI specific {@link ResourceFilter filters}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Provider
@Named
@ExportAsService(ResourceFilterFactory.class)
public class SecurityResourceFilterFactory implements ResourceFilterFactory {

	private final SiteService siteService;
	private final JiraAuthenticationContext authenticationContext;
	private final GlobalPermissionManager globalPermissionManager;

	@Inject
	public SecurityResourceFilterFactory(SiteService siteService, @ComponentImport JiraAuthenticationContext authenticationContext,
	                                     @ComponentImport GlobalPermissionManager globalPermissionManager) {
		this.siteService = siteService;
		this.authenticationContext = authenticationContext;
		this.globalPermissionManager = globalPermissionManager;
	}

	@Override
	public final List<ResourceFilter> create(AbstractMethod am) {
		if (am.getResource().getResourceClass().getPackage().getName().startsWith("org.marvelution.jji.rest")) {
			return singletonList(new AdminRequiredResourceFilter(am, authenticationContext, globalPermissionManager));
		}
		return emptyList();
	}
}
