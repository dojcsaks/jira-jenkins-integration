/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.rest.security.AdminRequired;

/**
 * REST resource for {@link Job}s
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@AdminRequired
@Path("job")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JobResource {

	private final JobService jobService;

	public JobResource(JobService jobService) {
		this.jobService = jobService;
	}

	/**
	 * Get all the Jobs available
	 *
	 * @param includeDeleted flag where to include or exclude deleted jobs
	 * @return collections of Jobs
	 */
	@GET
	public List<Job> getAll(@QueryParam("includeDeleted") @DefaultValue("false") boolean includeDeleted) {
		return jobService.getAll(includeDeleted);
	}

	/**
	 * Get a single Job by its Id
	 *
	 * @param jobId the job Id
	 * @return 200 OK (With the Job) if there is a job with the given id, 404 NOT FOUND otherwise
	 */
	@GET
	@Path("{jobId}")
	public Job getJob(@PathParam("jobId") String jobId) {
		return jobService.getExisting(jobId);
	}

	/**
	 * Delete a Job with specified {@literal jobId} from the job cache
	 *
	 * @since 2.2.0
	 */
	@DELETE
	@Path("{jobId}")
	public void deleteJob(@PathParam("jobId") String jobId) {
		jobService.delete(jobId);
	}

	/**
	 * Trigger the synchronization of a specific job
	 */
	@PUT
	@Path("{jobId}/sync")
	public void synchronizeJob(@PathParam("jobId") String jobId) {
		jobService.synchronize(jobId);
	}

	/**
	 * Enable or Disable the auto linking of the job
	 *
	 * @param jobId   the id of the job
	 * @param enabled the flag whether the job is enabled for synchronization
	 */
	@POST
	@Path("{jobId}/link/{enabled}")
	public void enableLink(@PathParam("jobId") String jobId, @PathParam("enabled") boolean enabled) {
		jobService.enable(jobId, enabled);
	}

	/**
	 * Remove all the builds of a job
	 *
	 * @param jobId the id of the job to remove all the builds from
	 */
	@DELETE
	@Path("{jobId}/builds")
	public void deleteAllBuilds(@PathParam("jobId") String jobId) {
		jobService.deleteAllBuilds(jobId);
	}

	/**
	 * Mark all the builds of a job as deleted and start a full sync to rebuild the build cache
	 *
	 * @param jobId the job Id to restart the cache on
	 */
	@POST
	@Path("{jobId}/rebuild")
	public void rebuildJobCache(@PathParam("jobId") String jobId) {
		jobService.rebuildCache(jobId);
	}
}
