/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.web.resource;

import java.util.Optional;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableNumber;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

/**
 * {@link WebResourceDataProvider} that provides the site synchronization refresh interval for the UI.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class SiteRefreshIntervalWebResourceDataProvider implements WebResourceDataProvider {

	private static final long DEFAULT_INTERVAL = 30000;

	@Override
	public Jsonable get() {
		return new JsonableNumber(Optional.ofNullable(System.getProperty("jenkins.site.refresh.interval"))
		                                  .map(Long::parseLong)
		                                  .orElse(DEFAULT_INTERVAL));
	}
}
