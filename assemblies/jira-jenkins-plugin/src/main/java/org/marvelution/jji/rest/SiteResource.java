/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteStatus;
import org.marvelution.jji.rest.security.AdminRequired;

/**
 * REST resource for {@link Site}s
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@AdminRequired
@Path("site")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SiteResource {

	private final SiteService siteService;

	public SiteResource(SiteService siteService) {
		this.siteService = siteService;
	}

	/**
	 * Get all the sites available
	 *
	 * @return collection of all the sites
	 */
	@GET
	public List<Site> getAll(@QueryParam("includeJobs") @DefaultValue("false") boolean includeJobs) {
		return siteService.getAll(includeJobs);
	}

	/**
	 * Get a {@link Site} by its Id
	 *
	 * @param siteId the site id
	 * @return Ok Response in case there is a site with the given Id and a Not Found otherwise
	 */
	@GET
	@Path("{siteId}")
	public Site get(@PathParam("siteId") String siteId, @QueryParam("includeJobs") @DefaultValue("false") boolean includeJobs) {
		return siteService.getExisting(siteId, includeJobs);
	}

	/**
	 * Adds a new site
	 *
	 * @param site the new site to add
	 */
	@POST
	public Site addSite(Site site) {
		return siteService.add(site);
	}

	/**
	 * Updates a site
	 *
	 * @param siteId the id of the site to update, must match the id with the site parameter
	 * @param site   the site details
	 */
	@POST
	@Path("{siteId}")
	public Site updateSite(@PathParam("siteId") String siteId, Site site) {
		return siteService.update(siteId, site);
	}

	/**
	 * Delete a site
	 *
	 * @param siteId the id of the site to delete
	 */
	@DELETE
	@Path("{siteId}")
	public void deleteSite(@PathParam("siteId") String siteId) {
		siteService.delete(siteId);
	}

	/**
	 * Sync the {@link Site} by its given Id
	 *
	 * @param siteId the id of the Site to sync
	 */
	@POST
	@Path("{siteId}/sync")
	public void synchronizeSite(@PathParam("siteId") String siteId) {
		siteService.synchronize(siteId);
	}

	@GET
	@Path("{siteId}/sync/status")
	public Site getSynchronizationStatus(@PathParam("siteId") String siteId) {
		return siteService.getSynchronizationStatus(siteId);
	}

	/**
	 * Enable or Disable the auto linking of the site
	 *
	 * @param siteId  the id of the {@link Site}
	 * @param enabled the flag whether new jobs should be automatically linked.
	 */
	@POST
	@Path("{siteId}/autolink/{enabled}")
	public void enableAutoLinkNewJobs(@PathParam("siteId") String siteId, @PathParam("enabled") boolean enabled) {
		siteService.automaticallyEnableNewJobs(siteId, enabled);
	}

	/**
	 * Check is the {@link Site} is online and accessible
	 *
	 * @param siteId the id of the {@link Site}
	 * @return {@link SiteStatus}
	 */
	@GET
	@Path("{siteId}/status")
	public SiteStatus siteStatus(@PathParam("siteId") String siteId) {
		return siteService.getSiteStatus(siteId);
	}

	/**
	 * Deletes all the Jobs that are marked as deleted on the site with specified {@literal siteId}
	 *
	 * @since 2.2.0
	 */
	@DELETE
	@Path("{siteId}/jobs")
	public void removeJobs(@PathParam("siteId") String siteId) {
		siteService.deleteDeletedJobs(siteId);
	}
}
