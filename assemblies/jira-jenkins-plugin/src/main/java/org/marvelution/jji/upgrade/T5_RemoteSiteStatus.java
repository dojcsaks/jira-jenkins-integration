/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.upgrade;

import java.util.Collection;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.SiteClient;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.data.services.api.StateService;
import org.marvelution.jji.model.Site;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link PluginUpgradeTask} implementation to update flags stored that related to remote site state link back-link and crumb usages
 * configuration.
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
@Named
@ExportAsService(PluginUpgradeTask.class)
public class T5_RemoteSiteStatus extends AbstractUpgradeTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(T5_RemoteSiteStatus.class);
	private final SiteService siteService;
	private final SiteClient siteClient;

	@Inject
	public T5_RemoteSiteStatus(StateService stateService, SiteService siteService, SiteClient siteClient) {
		super(stateService);
		this.siteService = siteService;
		this.siteClient = siteClient;
	}

	@Override
	public Collection<Message> doUpgrade() throws Exception {
		for (Site site : siteService.getAll()) {
			site.setJenkinsPluginInstalled(siteClient.isJenkinsPluginInstalled(site));
			site.setUseCrumbs(siteClient.isCrumbSecurityEnabled(site));
			LOGGER.info("Updated site status of {}", site.getName());
			siteService.save(site);
		}
		return null;
	}
}
