/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest.jackson;

import javax.annotation.Nullable;

import org.marvelution.jji.model.Obfuscate;

import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.introspect.Annotated;
import org.codehaus.jackson.map.introspect.NopAnnotationIntrospector;

/**
 * Custom {@link AnnotationIntrospector} to support Obfuscation using the {@link Obfuscate}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class ObfuscationAnnotationIntrospector extends NopAnnotationIntrospector {

	@Override
	@Nullable
	public Object findSerializer(Annotated am) {
		Obfuscate annotation = am.getAnnotated().getDeclaredAnnotation(Obfuscate.class);
		if (annotation != null) {
			return new ObfuscationSerializer(annotation);
		}
		return null;
	}

}
