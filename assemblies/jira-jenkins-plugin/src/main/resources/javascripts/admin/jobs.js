/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define('jira-jenkins-integration/admin/jobs', [
	'jquery',
	'aui/flag',
	'jira-jenkins-integration/utils'
], function (jquery, flag, utils) {
	'use strict';

	var DELETE_DIALOG_ID = '#delete-job-dialog';

	var jobs = {

		init: function () {
			jquery(document).on('click', '.job-sync-icon', function (event) {
				jobs.doActionForJob(event, jobs.syncJob);
			});
			jquery(document).on('click', '.link-job', function (event) {
				if (!jquery(event.target).is(':disabled')) {
					jobs.doActionForJob(event, function (jobId) {
						jobs.linkJob(jobId, jquery(event.target).is(':checked'));
					});
				}
			});
			jquery(document).on('click', '.delete-job', function (event) {
				jobs.doDropdownActionForJob(event, jobs.deleteJob);
			});
			jquery(document).on('click', '.delete-job-builds', function (event) {
				jobs.doDropdownActionForJob(event, jobs.deleteJobBuilds);
			});
			jquery(document).on('click', '.restart-job-sync', function (event) {
				jobs.doDropdownActionForJob(event, jobs.restartJobSync);
			});
		},

		getJobContainerSelector: function (jobId) {
			return '[jenkins-job-id="' + jobId + '"]';
		},

		getJobLinkCheckbox: function (jobId) {
			return jquery(jobs.getJobContainerSelector(jobId) + ' .link-job');
		},

		getJobName: function (jobId) {
			return jquery(jobs.getJobContainerSelector(jobId) + ' td a').first().text();
		},

		getJobSyncIcon: function (jobId) {
			return jquery(jobs.getJobContainerSelector(jobId) + ' .job-sync-icon');
		},

		getJobSyncStatus: function (jobId) {
			return jquery(jobs.getJobContainerSelector(jobId) + ' .job-sync-status');
		},

		handleJobSyncStatus: function (job) {
			var syncIcon = jobs.getJobSyncIcon(job.id);
			if (syncIcon.length) {
				if (job.deleted || job.progress === undefined || job.progress.finished) {
					syncIcon.removeClass('active');
					jobs.showJobSyncStatus(job.id, job.lastBuild);
					if (job.deleted) {
						jquery(jobs.getJobContainerSelector(job.id)).addClass('deleted-job');
					}
					return false;
				} else {
					var message = AJS.I18n.getText('job.sync.status.message', job.progress.jobCount, job.progress.buildCount,
							job.progress.issueCount);
					if (job.progress.errorCount > 0) {
						message += ', ' + AJS.I18n.getText('job.sync.status.message.error', job.progress.errorCount);
					}
					syncIcon.addClass('active');
					jobs.showJobSyncStatus(job.id, message);
					return true;
				}
			} else {
				return 'new';
			}
		},

		doActionForJob: function (event, callback) {
			var jobId = jquery(event.target).closest('[jenkins-job-id]').attr('jenkins-job-id');
			if (jobId !== undefined) {
				callback(jobId)
			}
		},

		doDropdownActionForJob: function (event, callback) {
			var jobId = jquery(event.target).closest('[jenkins-job-id]').attr('jenkins-job-id');
			if (jobId !== undefined) {
				callback(jobId)
			}
		},

		showJobSyncStatus: function (jobId, status, successful) {
			var statusContainer = jobs.getJobSyncStatus(jobId);
			if (successful === undefined || successful) {
				statusContainer.removeClass('error');
			} else {
				statusContainer.addClass('error');
			}
			statusContainer.empty().append(status);
		},

		linkJob: function (jobId, enabled) {
			jquery.ajax({
				type: 'POST',
				dataType: 'json',
				contentType: 'application/json',
				url: AJS.contextPath() + '/rest/jenkins/1.0/job/' + jobId + '/link/' + enabled,
				success: function () {
					jobs.syncJob(jobId);
				}
			}).error(function () {
				flag({
					type: 'error',
					title: AJS.I18n.getText('job.link.failed.title', jobs.getJobName(jobId)),
					body: AJS.I18n.getText('job.link.failed.message')
				});
				jobs.getJobLinkCheckbox(jobId).removeClass('checked').attr('aria-checked', 'false');
			});
		},

		syncJob: function (jobId) {
			if (jobs.getJobSyncIcon(jobId).hasClass('active') || !jobs.getJobLinkCheckbox(jobId).is(':checked')) {
				// Already syncing or job is not linked
				return;
			}
			jquery.ajax({
				type: 'PUT',
				dataType: 'json',
				url: AJS.contextPath() + '/rest/jenkins/1.0/job/' + jobId + '/sync',
				success: function () {
					jobs.getJobSyncIcon(jobId).addClass('active');
				}
			}).error(function (err) {
				jobs.showJobSyncStatus(jobId, err.responseText, false);
			});
		},

		deleteJob: function (id) {
			var name = jquery(jobs.getJobContainerSelector(id) + ' td:nth-child(2)').text();
			AJS.dialog2(JJI.Templates.Admin.Jobs.deleteJobDialog({'name': name})).show();
			jquery(DELETE_DIALOG_ID + ' .aui-button-link').on('click', function () {
				AJS.dialog2(DELETE_DIALOG_ID).hide();
				return false;
			});
			jquery(DELETE_DIALOG_ID + ' .aui-button-primary').on('click', function () {
				utils.blanketDialog(DELETE_DIALOG_ID);
				jquery.ajax({
					type: 'DELETE',
					url: AJS.contextPath() + '/rest/jenkins/1.0/job/' + id,
					success: function () {
						flag({
							type: 'success',
							close: 'auto',
							title: AJS.I18n.getText('job.delete.success.message', name)
						});
						AJS.dialog2(DELETE_DIALOG_ID).hide();
						jquery(jobs.getJobContainerSelector(id)).remove();
					}
				}).error(function () {
					utils.unblanketDialog(DELETE_DIALOG_ID);
					flag({
						type: 'error',
						title: AJS.I18n.getText('job.delete.failed.title', name),
						body: AJS.I18n.getText('job.delete.failed.message')
					});
					AJS.dialog2(DELETE_DIALOG_ID).hide();
				});
				return false;
			});
		},

		deleteJobBuilds: function (jobId) {
			jquery.ajax({
				type: 'DELETE',
				dataType: 'json',
				url: AJS.contextPath() + '/rest/jenkins/1.0/job/' + jobId + '/builds',
				success: function () {
					jobs.showJobSyncStatus(jobId, '0', true);
				}
			}).error(function (err) {
				jobs.showJobSyncStatus(jobId, err.responseText, false);
			});
		},

		restartJobSync: function (jobId) {
			jquery.ajax({
				type: 'POST',
				dataType: 'json',
				url: AJS.contextPath() + '/rest/jenkins/1.0/job/' + jobId + '/rebuild',
				success: function () {
					jobs.showJobSyncStatus(jobId, '0', true);
					jobs.getJobSyncIcon(jobId).addClass('active');
				}
			}).error(function (err) {
				jobs.showJobSyncStatus(jobId, err.responseText, false);
			});
		}

	};

	return jobs;

});
