/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.panels;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.BuildLinkService;
import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.StateService;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.BuildIssueFilter;
import org.marvelution.jji.model.Job;

import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.Issue;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Collections.singletonList;
import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsFirst;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;

/**
 * CI Build Panel Helper
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class BuildPanelHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(BuildPanelHelper.class);
	private final ConfigurationService pluginConfiguration;
	private final StateService stateService;
	private final BuildService buildService;
	private final BuildLinkService buildLinkService;
	private final DateTimeFormatter dateTimeFormatter;
	private final TemplateRenderer templateRenderer;

	@Inject
	public BuildPanelHelper(ConfigurationService pluginConfiguration, StateService stateService, BuildService buildService,
	                        BuildLinkService buildLinkService, @ComponentImport DateTimeFormatter dateTimeFormatter,
	                        @ComponentImport TemplateRenderer templateRenderer) {
		this.pluginConfiguration = pluginConfiguration;
		this.stateService = stateService;
		this.buildService = buildService;
		this.buildLinkService = buildLinkService;
		this.dateTimeFormatter = dateTimeFormatter;
		this.templateRenderer = templateRenderer;
	}

	/**
	 * Returns whether the panel should be shown for the given issue and current user.
	 */
	boolean showPanel(Issue issue) {
		return stateService.areFeaturesEnabledFor(issue);
	}

	/**
	 * Returns a list of all the {@link BuildAction}s related to the specified {@link Issue}.
	 */
	List<BuildAction> getBuildActions(Issue issue) {
		LOGGER.debug("Looking for builds related to issue [{}]", issue.getKey());
		BuildIssueFilter filter = new BuildIssueFilter();
		filter.getInIssueKeys().add(issue.getKey());
		List<BuildAction> actions = buildService.getLatestBuildsByFilter(pluginConfiguration.getMaximumBuildsPerPage(), filter).stream()
		                                        .map(build -> getBuildActionHtml(build)
				                                        .map(html -> new BuildAction(html, build.getBuildDate()))
				                                        .orElse(null))
		                                        .filter(Objects::nonNull)
		                                        .sorted(nullsFirst(comparing(BuildAction::getTimePerformed)).reversed())
		                                        .collect(toList());
		return actions.isEmpty() ? singletonList(BuildAction.NO_BUILDS_ACTION) : actions;
	}

	/**
	 * Generate a HTML View for the {@link Build} given
	 *
	 * @param build the {@link Build} to generate the HTML view for
	 * @return the HTML string, may be {@code empty} but not {@code null}
	 */
	private Optional<String> getBuildActionHtml(Build build) {
		Job job = build.getJob();
		Map<String, Object> context = new HashMap<>();
		context.put("baseUrl", pluginConfiguration.getJIRABaseUrl().toASCIIString());
		context.put("job", job);
		context.put("jobUrl", job.getDisplayUrl());
		context.put("build", build);
		context.put("buildTimestamp", dateTimeFormatter.forLoggedInUser().withStyle(DateTimeStyle.RELATIVE).format(build.getBuildDate()));
		context.put("buildUrl", build.getDisplayUrl());
		if (buildLinkService.getIssueLinkCount(build) <= 5) {
			context.put("issueKeys", new ArrayList<>(buildLinkService.getRelatedIssueKeys(build)));
		} else {
			context.put("projectKeys", new ArrayList<>(buildLinkService.getRelatedProjectKeys(build)));
		}
		try {
			StringWriter writer = new StringWriter();
			templateRenderer.render("/templates/panels/jenkins-build.vm", context, writer);
			return of(writer.toString());
		} catch (IOException e) {
			LOGGER.warn("Failed to render html for build {} -> {}", build.getId(), e.getMessage());
			return empty();
		}
	}

}
