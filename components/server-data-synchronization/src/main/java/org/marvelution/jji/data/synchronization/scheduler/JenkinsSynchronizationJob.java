/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization.scheduler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.data.synchronization.api.SynchronizationService;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.config.JobRunnerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Jenkins Scheduled Synchronization Job
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class JenkinsSynchronizationJob implements JobRunner {

	static final JobRunnerKey JOB_KEY = JobRunnerKey.of(JenkinsSynchronizationJob.class.getName());
	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsSynchronizationJob.class);
	private final SiteService siteService;
	private final SynchronizationService synchronizationService;

	@Inject
	public JenkinsSynchronizationJob(SiteService siteService, SynchronizationService synchronizationService) {
		this.siteService = siteService;
		this.synchronizationService = synchronizationService;
	}

	@Nullable
	@Override
	public JobRunnerResponse runJob(@Nonnull JobRunnerRequest jobRunnerRequest) {
		try {
			siteService.getAll().forEach(synchronizationService::synchronize);
			return JobRunnerResponse.success();
		} catch (Throwable t) {
			LOGGER.warn("Failed to execute the Jenkins Scheduled Job; {}", t.getMessage());
			return JobRunnerResponse.failed(t);
		}
	}
}
