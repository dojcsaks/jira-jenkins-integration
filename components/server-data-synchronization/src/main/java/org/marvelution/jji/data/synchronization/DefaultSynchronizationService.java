/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.model.OperationId;
import org.marvelution.jji.model.Syncable;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static com.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static java.util.concurrent.Executors.newFixedThreadPool;

/**
 * Synchronization Service
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultSynchronizationService extends BaseSynchronizationService<DefaultProgress> {

	static final String SYNC_LOCK_NAME = DefaultSynchronizationService.class.getName() + ".synchronize";
	static final String PROGRESS_CACHE_NAME = DefaultSynchronizationService.class.getName() + ".syncProgress";
	static final CacheSettings PROGRESS_CACHE_SETTINGS = new CacheSettingsBuilder()
			.expireAfterWrite(1L, TimeUnit.HOURS)
			.flushable()
			.remote()
			.build();
	private final ExecutorService executorService;
	private final DelegatingSynchronizationOperation synchronizationOperation;
	private final ClusterLockService lockService;
	private final Cache<String, DefaultProgress> progressCache;

	@Inject
	public DefaultSynchronizationService(DelegatingSynchronizationOperation synchronizationOperation,
	                                     @ComponentImport CacheManager cacheManager, @ComponentImport ClusterLockService lockService) {
		this.executorService = newFixedThreadPool(5, namedThreadFactory("JenkinsSynchronizerExecutorServiceThread"));
		this.synchronizationOperation = synchronizationOperation;
		this.lockService = lockService;
		progressCache = cacheManager.getCache(PROGRESS_CACHE_NAME, null, PROGRESS_CACHE_SETTINGS);
	}

	@Override
	protected void enqueueSyncable(Syncable syncable, DefaultProgress progress) {
		executorService.submit(() -> {
			try {
				progress.started();
				if (progress.stopRequested()) {
					return;
				}
				synchronizationOperation.synchronize(syncable, progress);
			} catch (Throwable e) {
				progress.addError(e);
				String message = (e.getMessage() != null ? e.getMessage() : e.toString());
				logger.warn("Failed to synchronize {}: {}", syncable.getOperationId().getId(), message, e);
			} finally {
				progress.finished();
			}
		});
	}

	@Override
	protected Lock obtainLock() {
		return lockService.getLockForName(SYNC_LOCK_NAME);
	}

	@Override
	protected DefaultProgress createProgress(OperationId operationId) {
		DefaultProgress progress = new DefaultProgress();
		progressCache.put(operationId.getId(), progress);
		return progress;
	}

	@Override
	protected Optional<DefaultProgress> getProgress(OperationId operationId) {
		try {
			// Explicitly load to DefaultProgress to trigger casting errors early.
			DefaultProgress progress = progressCache.get(operationId.getId());
			if (progress != null && !DefaultProgress.class.equals(progress.getClass())) {
				throw new ClassCastException();
			}
			return ofNullable(progress);
		} catch (ClassCastException e) {
			// Loading an item that was cached on a previous version of the add-on can generate ClassCastExceptions
			progressCache.remove(operationId.getId());
			return empty();
		}
	}

	/**
	 * Cleanup just before the bean is set to be destroyed.
	 * This will trigger an all-stop for all ongoing synchronization jobs without logging errors.
	 */
	@PreDestroy
	public void destroy() {
		executorService.shutdownNow();
	}

}
