/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;

import org.marvelution.jji.model.OperationId;
import org.marvelution.jji.model.Progress;
import org.marvelution.jji.model.SyncProgress;
import org.marvelution.jji.model.Syncable;
import org.marvelution.testing.TestSupport;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.marvelution.jji.data.synchronization.DefaultSynchronizationService.PROGRESS_CACHE_NAME;
import static org.marvelution.jji.data.synchronization.DefaultSynchronizationService.PROGRESS_CACHE_SETTINGS;
import static org.marvelution.jji.data.synchronization.DefaultSynchronizationService.SYNC_LOCK_NAME;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Testcase for {@link DefaultSynchronizationService}
 *
 * @author Mark Rekveld
 * @since 2.3.0
 */
public class DefaultSynchronizationServiceTest extends TestSupport {

	@Mock
	private CacheManager cacheManager;
	@Mock
	private Cache<String, DefaultProgress> progressCache;
	@Mock
	private ClusterLockService lockService;
	@Mock
	private ClusterLock clusterLock;
	@Mock
	private DelegatingSynchronizationOperation synchronizationOperation;
	private DefaultSynchronizationService synchronizer;
	private OperationId operationId;

	@Before
	public void setUp() throws Exception {
		operationId = OperationId.of(testName.getMethodName());
		when(cacheManager.getCache(PROGRESS_CACHE_NAME, null, PROGRESS_CACHE_SETTINGS)).then(invocation -> progressCache);
		when(lockService.getLockForName(SYNC_LOCK_NAME)).thenReturn(clusterLock);
		synchronizer = new DefaultSynchronizationService(synchronizationOperation, cacheManager, lockService);
	}

	@Test
	public void testSynchronize() throws Exception {
		when(progressCache.get(operationId.getId())).thenReturn(null);

		DummySyncable syncable = new DummySyncable(operationId);
		synchronizer.synchronize(syncable);

		verify(lockService).getLockForName(SYNC_LOCK_NAME);
		verify(clusterLock).lock();
		verify(progressCache).get(operationId.getId());
		verify(progressCache).put(eq(operationId.getId()), any(DefaultProgress.class));
		verify(clusterLock).unlock();
		TimeUnit.SECONDS.sleep(1);
		verify(synchronizationOperation).synchronize(eq(syncable), any(DefaultProgress.class));
	}

	@Test
	public void testSynchronize_SyncProgressFinished() throws Exception {
		DefaultProgress progress = new DefaultProgress();
		progress.finished();
		when(progressCache.get(operationId.getId())).thenReturn(progress);

		DummySyncable syncable = new DummySyncable(operationId);
		synchronizer.synchronize(syncable);

		verify(lockService).getLockForName(SYNC_LOCK_NAME);
		verify(clusterLock).lock();
		verify(progressCache).get(operationId.getId());
		verify(progressCache).put(eq(operationId.getId()), any(DefaultProgress.class));
		verify(clusterLock).unlock();
		TimeUnit.SECONDS.sleep(1);
		verify(synchronizationOperation).synchronize(eq(syncable), any(DefaultProgress.class));
	}

	@Test
	public void testSynchronize_DontSyncWhenInProgress() throws Exception {
		DefaultProgress progress = new DefaultProgress();
		when(progressCache.get(operationId.getId())).thenReturn(progress);

		synchronizer.synchronize(new DummySyncable(operationId));

		verify(lockService).getLockForName(SYNC_LOCK_NAME);
		verify(clusterLock).lock();
		verify(progressCache).get(operationId.getId());
		verify(clusterLock).unlock();
		verify(progressCache, never()).put(eq(operationId.getId()), any(DefaultProgress.class));
		TimeUnit.SECONDS.sleep(1);
		verify(synchronizationOperation, never()).synchronize(any(Syncable.class), any(DefaultProgress.class));
	}

	@Test
	public void testSynchronize_DontSyncWhenNotEnabled() throws Exception {
		DefaultProgress progress = new DefaultProgress();
		when(progressCache.get(operationId.getId())).thenReturn(progress);

		synchronizer.synchronize(new DummySyncable(operationId, false));

		verify(lockService).getLockForName(SYNC_LOCK_NAME);
		verify(clusterLock).lock();
		verify(clusterLock).unlock();
		verify(progressCache, never()).get(operationId.getId());
		verify(progressCache, never()).put(eq(operationId.getId()), any(DefaultProgress.class));
		TimeUnit.SECONDS.sleep(1);
		verify(synchronizationOperation, never()).synchronize(any(Syncable.class), any(DefaultProgress.class));
	}

	@Test
	public void testSynchronize_RecoverClassCastException() throws Exception {
		// Use a different classloader to populate the cache to simulate add-on upgrades.
		ClassLoader newLoader = new URLClassLoader(new URL[] { DefaultProgress.class.getProtectionDomain().getCodeSource().getLocation(),
		                                                       Progress.class.getProtectionDomain().getCodeSource().getLocation() }, null);
		Class<?> defaultProgressClass = newLoader.loadClass(DefaultProgress.class.getName());
		when(progressCache.get(operationId.getId())).then(invocation -> defaultProgressClass.newInstance());

		DummySyncable syncable = new DummySyncable(operationId);
		synchronizer.synchronize(syncable);

		verify(lockService).getLockForName(SYNC_LOCK_NAME);
		verify(clusterLock).lock();
		verify(progressCache).get(operationId.getId());
		verify(progressCache).remove(operationId.getId());
		verify(progressCache).put(eq(operationId.getId()), any(DefaultProgress.class));
		verify(clusterLock).unlock();
		TimeUnit.SECONDS.sleep(1);
		verify(synchronizationOperation).synchronize(eq(syncable), any(DefaultProgress.class));
	}

	private static class DummySyncable implements Syncable {

		final OperationId operationId;
		final boolean enabled;
		String id = UUID.randomUUID().toString();

		DummySyncable(OperationId operationId) {
			this(operationId, true);
		}

		DummySyncable(OperationId operationId, boolean enabled) {
			this.operationId = operationId;
			this.enabled = enabled;
		}

		@Override
		public OperationId getOperationId() {
			return operationId;
		}

		@Override
		public boolean isEnabled() {
			return enabled;
		}

		@Nullable
		@Override
		public SyncProgress getProgress() {
			return null;
		}

		@Override
		public void setProgress(@Nullable SyncProgress progress) {

		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public DummySyncable setId(String id) {
			this.id = id;
			return this;
		}
	}

}
