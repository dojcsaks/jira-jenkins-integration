/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.indexing;

import java.util.HashSet;
import java.util.Set;

import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Result;
import org.marvelution.jji.model.Site;
import org.marvelution.testing.TestSupport;

import com.atlassian.jira.index.EntitySearchExtractor;
import com.atlassian.jira.index.IssueSearchExtractor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Fieldable;
import org.hamcrest.Description;
import org.hamcrest.DiagnosingMatcher;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.marvelution.jji.indexing.JenkinsIssueSearchExtractor.WORST_RESULT_FIELD_NAME;
import static org.marvelution.jji.indexing.JenkinsIssueSearchExtractor.fieldNameForResult;
import static org.marvelution.jji.model.Result.ABORTED;
import static org.marvelution.jji.model.Result.FAILURE;
import static org.marvelution.jji.model.Result.NOT_BUILT;
import static org.marvelution.jji.model.Result.SUCCESS;
import static org.marvelution.jji.model.Result.UNKNOWN;
import static org.marvelution.jji.model.Result.UNSTABLE;

import static java.util.Arrays.stream;
import static java.util.Collections.singleton;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Testcase for the {@link JenkinsIssueSearchExtractor}
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
public class JenkinsIssueSearchExtractorTest extends TestSupport {

	private static final String[] FIELD_NAMES = stream(Result.values()).map(JenkinsIssueSearchExtractor::fieldNameForResult)
	                                                                   .toArray(String[]::new);
	@Mock
	private ConfigurationService pluginConfiguration;
	@Mock
	private JobService jobService;
	@Mock
	private BuildService buildService;
	@Mock
	private DoubleConverter doubleConverter;
	@Mock
	private EntitySearchExtractor.Context<Issue> context;
	private IssueSearchExtractor issueSearchExtractor;

	@Before
	public void setUp() throws Exception {
		when(doubleConverter.getStringForLucene(any(Double.class))).then(invocation -> String.valueOf(invocation.getArguments()[0]));
		issueSearchExtractor = new JenkinsIssueSearchExtractor(pluginConfiguration, jobService, buildService, doubleConverter);
		Issue issue = mock(Issue.class);
		when(issue.getKey()).thenReturn("JJI-1");
		when(context.getEntity()).thenReturn(issue);
		when(context.getIndexName()).thenReturn(testName.getMethodName());
	}

	@Test
	public void testIndexEntity_NoBuilds() throws Exception {
		when(buildService.getByIssueKey("JJI-1")).thenReturn(new HashSet<>());
		Document document = new Document();
		Set<String> fields = issueSearchExtractor.indexEntity(context, document);
		assertThat(fields, hasSize(0));
		assertThat(document.getFields(), hasSize(0));
		verify(buildService, times(1)).getByIssueKey("JJI-1");
		verify(context.getEntity(), times(1)).getKey();
		verifyZeroInteractions(pluginConfiguration);
	}

	@Test
	public void testIndexEntity() throws Exception {
		when(pluginConfiguration.useJobLatestBuildResultInIndexes()).thenReturn(false);
		when(buildService.getByIssueKey("JJI-1")).thenAnswer(invocation -> {
			Set<Build> builds = new HashSet<>();
			for (Result result : Result.values()) {
				Build build = new Build().setId(Integer.toString(builds.size() + 1))
				                         .setJob(new Job().setId("1")).setNumber(builds.size() + 1);
				build.setResult(result);
				builds.add(build);
			}
			return builds;
		});
		Document document = new Document();
		Set<String> fields = issueSearchExtractor.indexEntity(context, document);
		assertThat(fields, hasSize(7));
		assertThat(document.getFields(), hasSize(7));
		for (String fieldName : FIELD_NAMES) {
			assertThat(fields, hasItem(fieldName));
			assertThat(document.getFields(), hasItem(fieldable(fieldName, "1.0")));
		}
		assertThat(fields, hasItem(WORST_RESULT_FIELD_NAME));
		assertThat(document.getFields(), hasItem(fieldable(WORST_RESULT_FIELD_NAME, FAILURE.key())));
		verify(buildService, times(1)).getByIssueKey("JJI-1");
		verify(context.getEntity(), times(2)).getKey();
		verify(pluginConfiguration, times(1)).useJobLatestBuildResultInIndexes();
	}

	@Test
	public void testIndexEntity_OnlySuccessfulBuilds() throws Exception {
		testIndexEntitySingleBuild(SUCCESS);
	}

	@Test
	public void testIndexEntity_OnlyFailingBuilds() throws Exception {
		testIndexEntitySingleBuild(Result.FAILURE);
	}

	@Test
	public void testIndexEntity_OnlyUnstableBuilds() throws Exception {
		testIndexEntitySingleBuild(UNSTABLE);
	}

	@Test
	public void testIndexEntity_OnlyAbortedBuilds() throws Exception {
		testIndexEntitySingleBuild(ABORTED);
	}

	@Test
	public void testIndexEntity_OnlyNotBuiltBuilds() throws Exception {
		testIndexEntitySingleBuild(NOT_BUILT);
	}

	@Test
	public void testIndexEntity_OnlyUnknownBuilds() throws Exception {
		testIndexEntitySingleBuild(UNKNOWN);
	}

	private void testIndexEntitySingleBuild(Result result) throws Exception {
		when(pluginConfiguration.useJobLatestBuildResultInIndexes()).thenReturn(false);
		when(buildService.getByIssueKey("JJI-1")).thenAnswer(invocation -> {
			Build build = new Build().setId("1").setJob(new Job().setId("1")).setNumber(1);
			build.setResult(result);
			return singleton(build);
		});
		Document document = new Document();
		Set<String> fields = issueSearchExtractor.indexEntity(context, document);
		assertThat(fields, hasSize(7));
		assertThat(document.getFields(), hasSize(7));
		assertThat(fields, hasItem(fieldNameForResult(result)));
		for (String fieldName : FIELD_NAMES) {
			String expectedValue = fieldName.equals(fieldNameForResult(result)) ? "1.0" : "0.0";
			assertThat(document.getFields(), hasItem(fieldable(fieldName, expectedValue)));
		}
		assertThat(fields, hasItem(WORST_RESULT_FIELD_NAME));
		assertThat(document.getFields(), hasItem(fieldable(WORST_RESULT_FIELD_NAME, result.key())));
		verify(buildService, times(1)).getByIssueKey("JJI-1");
		verify(context.getEntity(), times(2)).getKey();
		verify(pluginConfiguration, times(1)).useJobLatestBuildResultInIndexes();
	}

	@Test
	public void testIndexEntity_UseJobLatestBuildWhenIndexing() throws Exception {
		when(pluginConfiguration.useJobLatestBuildResultInIndexes()).thenReturn(true);
		Job job = new Job().setId("1").setName("Indexing Test").setSite(new Site().setId("1"));
		job.setLastBuild(10);
		when(jobService.get(job.getId())).thenReturn(of(job));
		when(buildService.getByIssueKey("JJI-1")).thenAnswer(invocation -> stream(Result.values())
				.map(result -> {
					int i = result.ordinal() + 1;
					Build build = new Build().setId(Integer.toString(i)).setJob(job).setNumber(i);
					build.setResult(result);
					return build;
				})
				.collect(toSet()));
		when(buildService.get(job, job.getLastBuild())).thenAnswer(invocation -> {
			int i = Result.values().length + 1;
			Build build = new Build().setId(Integer.toString(i)).setJob(job).setNumber(i);
			build.setResult(SUCCESS);
			return of(build);
		});
		Document document = new Document();
		Set<String> fields = issueSearchExtractor.indexEntity(context, document);
		assertThat(fields, hasSize(7));
		assertThat(document.getFields(), hasSize(7));
		for (String fieldName : FIELD_NAMES) {
			assertThat(document.getFields(), hasItem(fieldable(fieldName, "1.0")));
		}
		assertThat(fields, hasItem(WORST_RESULT_FIELD_NAME));
		assertThat(document.getFields(), hasItem(fieldable(WORST_RESULT_FIELD_NAME, SUCCESS.key())));
		verify(buildService, times(1)).getByIssueKey("JJI-1");
		verify(buildService, times(1)).get(job, job.getLastBuild());
		verify(context.getEntity(), times(2)).getKey();
		verify(pluginConfiguration, times(1)).useJobLatestBuildResultInIndexes();
	}

	private Matcher<Fieldable> fieldable(String fieldName, String expectedValue) {
		return new DiagnosingMatcher<Fieldable>() {
			@Override
			protected boolean matches(Object item, Description mismatchDescription) {
				if (item == null) {
					mismatchDescription.appendText("null");
					return false;
				} else if (!(item instanceof Fieldable)) {
					mismatchDescription.appendValue(item).appendValue(" is a " + item.getClass().getName());
					return false;
				}
				Fieldable fieldable = (Fieldable) item;
				if (!fieldName.equals(fieldable.name())) {
					mismatchDescription.appendValue("field name does not match ").appendValue(fieldable.name());
				} else if (fieldable.isStored()) {
					mismatchDescription.appendValue("field should not be stored");
				} else if (!fieldable.isIndexed()) {
					mismatchDescription.appendValue("field should be indexed");
				} else if (fieldable.isTokenized()) {
					mismatchDescription.appendValue("field should not be tokenized");
				} else if (!fieldable.getOmitNorms()) {
					mismatchDescription.appendValue("field should omit norms");
				} else if (fieldable.isTermVectorStored()) {
					mismatchDescription.appendValue("field shouold not store term vector");
				} else if (fieldable.isBinary()) {
					mismatchDescription.appendValue("field should not be binary");
				} else {
					return expectedValue.equals(fieldable.stringValue());
				}
				return false;
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("fieldable for field ").appendValue(fieldName).appendText(" with value ").appendValue(expectedValue);
			}
		};
	}

}
