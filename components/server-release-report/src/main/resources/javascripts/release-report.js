/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('jira-jenkins-integration/release-report', [
	'underscore',
	'jquery',
	'aui/flag',
	'jira-jenkins-integration/utils'
], function (_, jquery, flag, utils) {
	'use strict';

	var DIALOG_ID = "#report-build-info-dialog";

	var report = {
		init: function () {
			jquery(document).on('click', 'td.cibuilds a[data-issue-key]', function () {
				var element = jquery(this);
				element.spin("small", {top: '0', left: '0'});
				var issueKey = element.data('issue-key');
				jquery.ajax({
					type: 'GET',
					dataType: 'json',
					url: AJS.contextPath() + '/rest/jenkins/1.0/release-report/build-info/' + issueKey,
					success: function (data) {
						element.spin(false);
						if (data !== undefined && data.length > 0) {
							report.showCiBuildsDialog(issueKey, data);
						} else {
							flag({
								type: 'info',
								title: AJS.I18n.getText('release.report.no.build.info.title', issueKey)
							});
						}
					}
				}).error(function () {
					element.spin(false);
					flag({
						type: 'error',
						title: AJS.I18n.getText('release.report.build.info.failed.title', issueKey)
					});
				});
			});
		},
		                
		showCiBuildsDialog: function (issueKey, jobs) {
			AJS.dialog2(JJI.Templates.ReleaseReport.buildInfoDialog({
				'issueKey': issueKey,
				'jobs': report.sortJobsAndBuilds(jobs)
			})).show();
			utils.addTimestampTooltip(jquery(DIALOG_ID));
			jquery(DIALOG_ID + ' .aui-button-link').off('click').on('click', function () {
				AJS.dialog2(DIALOG_ID).hide();
				return false;
			});
		},

		sortJobsAndBuilds: function(jobs) {
			jobs = _.sortBy(jobs, function(job) {
				return _.last(_.sortBy(job.builds, function(build) {
					return build.timestamp;
				})).timestamp;
			}).reverse();
			_.each(jobs, function(job) {
				job.builds = _.sortBy(job.builds, function(build) {
					return build.timestamp;
				}).reverse();
			});
			return jobs;
		}

	};

	return report;

});

require([
	'jira-jenkins-integration/release-report',
	'jquery'
], function (releaseReport, jquery) {
	'use strict';

	jquery(function () {
		releaseReport.init();
	});

});
