/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.UUID;

import org.marvelution.jji.data.access.model.v2.BuildEntity;
import org.marvelution.jji.data.access.model.v2.JobEntity;
import org.marvelution.jji.data.access.model.v2.SiteEntity;

import static java.util.Arrays.stream;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class EntityHelper {

	public static String newId() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	static <DAO extends AbstractModelDAO> void deleteSite(SiteEntity site, DAO dao) {
		stream(site.getJobs()).forEach(job -> deleteJob(job, dao));
		dao.deleteEntity(site);
	}

	static <DAO extends AbstractModelDAO> void deleteJob(JobEntity job, DAO dao) {
		stream(job.getBuilds()).forEach(build -> deleteBuild(build, dao));
		dao.deleteEntity(job);
	}

	static <DAO extends AbstractModelDAO> void deleteBuild(BuildEntity build, DAO dao) {
		dao.deleteEntity(build.getChangeSets());
		dao.deleteEntity(build.getIssueLinks());
		dao.deleteEntity(build.getTestResults());
		dao.deleteEntity(build);
	}
}
