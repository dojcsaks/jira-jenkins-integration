/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.TextResolver;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;

/**
 * {@link TextResolver} implementation that uses the {@link I18nResolver}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultTextResolver implements TextResolver {

	private final I18nResolver i18nResolver;

	@Inject
	public DefaultTextResolver(@ComponentImport I18nResolver i18nResolver) {
		this.i18nResolver = i18nResolver;
	}

	@Override
	public String getText(String key, Serializable... arguments) {
		return i18nResolver.getText(key, arguments);
	}
}
