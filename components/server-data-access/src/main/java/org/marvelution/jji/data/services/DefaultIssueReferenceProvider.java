/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.data.services.api.IssueReferenceProvider;
import org.marvelution.jji.model.IssueReference;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.project.Project;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static org.marvelution.jji.utils.KeyExtractor.extractProjectKeyFromIssueKey;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toSet;
import static javax.ws.rs.core.UriBuilder.fromUri;

/**
 * Default implementation of the {@link IssueReferenceProvider}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
@Named
public class DefaultIssueReferenceProvider implements IssueReferenceProvider {

	private final ConfigurationService configurationService;
	private final IssueManager issueManager;

	@Inject
	public DefaultIssueReferenceProvider(ConfigurationService configurationService, @ComponentImport IssueManager issueManager) {
		this.configurationService = configurationService;
		this.issueManager = issueManager;
	}

	@Override
	public Optional<IssueReference> getIssueReference(String key) {
		return of(key)
				.map(issueManager::getIssueObject)
				.map(this::toReference);
	}

	@Override
	public Set<IssueReference> getIssueReferences(Set<String> keys) {
		return keys.stream()
		           .map(issueManager::getIssueObject)
		           .filter(Objects::nonNull)
		           .map(this::toReference)
		           .collect(toSet());
	}

	@Override
	public void setIssueUrl(IssueReference reference) {
		reference.setIssueUrl(fromUri(configurationService.getJIRABaseUrl()).path("browse/{issueKey}").build(reference.getIssueKey()));
	}

	private IssueReference toReference(Issue issue) {
		return new IssueReference().setIssueKey(issue.getKey())
		                           .setProjectKey(ofNullable(issue.getProjectObject())
				                                          .map(Project::getKey)
				                                          .orElse(extractProjectKeyFromIssueKey(issue.getKey())));
	}
}
