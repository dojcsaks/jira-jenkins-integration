/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.Map;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.services.api.StateService;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.software.api.conditions.IsSoftwareProjectCondition;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static java.util.Collections.singletonMap;

/**
 * Default {@link StateService}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
@ExportAsDevService(StateService.class)
public class DefaultStateService implements StateService {

	private final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;
	private final IsSoftwareProjectCondition isSoftwareProjectCondition;
	private final ProjectManager projectManager;
	private final IssueManager issueManager;
	private final PluginRetrievalService pluginRetrievalService;

	@Inject
	public DefaultStateService(ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition,
	                           IsSoftwareProjectCondition isSoftwareProjectCondition,
	                           @ComponentImport ProjectManager projectManager, @ComponentImport IssueManager issueManager,
	                           @ComponentImport PluginRetrievalService pluginRetrievalService) {
		this.projectDevToolsIntegrationFeatureCondition = projectDevToolsIntegrationFeatureCondition;
		this.isSoftwareProjectCondition = isSoftwareProjectCondition;
		this.projectManager = projectManager;
		this.issueManager = issueManager;
		this.pluginRetrievalService = pluginRetrievalService;
	}

	@Override
	public String getAddonKey() {
		return getPlugin().getKey();
	}

	@Override
	public boolean isAddonEnabled() {
		return getPlugin().getPluginState() == PluginState.ENABLED;
	}

	@Override
	public boolean areFeaturesEnabledForIssue(String issueKey) {
		return areFeaturesEnabledFor(issueManager.getIssueObject(issueKey));
	}

	@Override
	public boolean areFeaturesEnabledForProject(String projectKey) {
		return areFeaturesEnabledFor(projectManager.getProjectObjByKey(projectKey));
	}

	@Override
	public boolean areFeaturesEnabledFor(@Nullable Object object) {
		if (object != null) {
			if (object instanceof Project) {
				Map<String, Object> browseContext = singletonMap(ProjectDevToolsIntegrationFeatureCondition.CONTEXT_KEY_PROJECT, object);
				return isSoftwareProjectCondition.shouldDisplay(browseContext) &&
						projectDevToolsIntegrationFeatureCondition.shouldDisplay(browseContext);
			} else if (object instanceof Issue) {
				return areFeaturesEnabledFor(((Issue) object).getProjectObject());
			}
		}
		return false;
	}

	private Plugin getPlugin() {
		return pluginRetrievalService.getPlugin();
	}
}
