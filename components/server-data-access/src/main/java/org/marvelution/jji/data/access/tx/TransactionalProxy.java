/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.tx;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;

import static java.util.Objects.requireNonNull;

/**
 * {@link InvocationHandler} implementation to execute all methods the wrapped bean within a transaction provided by
 * {@link ActiveObjects#executeInTransaction(TransactionCallback)}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
class TransactionalProxy implements InvocationHandler {

	private final ActiveObjects activeObjects;
	private final Object bean;

	TransactionalProxy(ActiveObjects activeObjects, Object bean) {
		this.activeObjects = requireNonNull(activeObjects, "activeObjects cannot be null");
		this.bean = requireNonNull(bean, "bean cannot be null");
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		return activeObjects.executeInTransaction(() -> {
			try {
				return method.invoke(bean, args);
			} catch (IllegalAccessException | InvocationTargetException e) {
				throw new TransactionalException(e);
			}
		});
	}

	private static final class TransactionalException extends RuntimeException {

		TransactionalException(Throwable cause) {
			super(cause);
		}

		public Throwable getThrowable() {
			Throwable cause = this.getCause();
			return cause instanceof InvocationTargetException ? cause.getCause() : cause;
		}
	}

}
