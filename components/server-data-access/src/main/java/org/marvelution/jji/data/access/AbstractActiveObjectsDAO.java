/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.marvelution.jji.data.access.model.v2.Entity;

import com.atlassian.activeobjects.external.ActiveObjects;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.marvelution.jji.data.access.EntityHelper.newId;

import static java.util.Arrays.stream;
import static java.util.Optional.ofNullable;

/**
 * Base implementation for DAO based on {@link ActiveObjects}
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
public abstract class AbstractActiveObjectsDAO<E extends Entity> {

	final Logger logger = LoggerFactory.getLogger(getClass());
	final ActiveObjects activeObjects;
	private final Class<E> entityClass;

	AbstractActiveObjectsDAO(ActiveObjects activeObjects, Class<E> entityClass) {
		this.activeObjects = activeObjects;
		this.entityClass = entityClass;
	}

	/**
	 * Returns an {@link Optional} reference to the {@link Entity} with the specified {@code id}
	 */
	Optional<E> getOne(String id) {
		return ofNullable(activeObjects.get(entityClass, id));
	}

	/**
	 * Returns an {@link Optional} reference to the {@link Entity} that is the first result of the specified {@link Query}
	 */
	Optional<E> findOne(Query query) {
		return find(query).findFirst();
	}

	/**
	 * Returns a {@link Stream} to {@link Entity Entities} that match the specified {@link Query}
	 */
	Stream<E> find(Query query) {
		return stream(activeObjects.find(entityClass, query));
	}

	/**
	 * Inserts a new {@link Entity} using the specified {@link Map} of fields and return it.
	 */
	E insert(NullCharacterRemovingHashMap mapping) {
		return insert(entityClass, mapping);
	}

	/**
	 * Inserts a new {@link Entity} of the specified {@code entityType} using the specified {@link Map} of fields and return it.
	 */
	<OE extends Entity> OE insert(Class<OE> entityType, NullCharacterRemovingHashMap mapping) {
		if (!mapping.containsKey(Entity.ID)) {
			mapping.put(Entity.ID, newId());
		}
		return activeObjects.create(entityType, mapping);
	}

	/**
	 * Update an {@link Entity} using the specified {@link Consumer updater} and return it in its new state.
	 */
	E update(String id, Consumer<E> updater) {
		return update(entityClass, id, updater);
	}

	/**
	 * Update an {@link Entity} of the specified {@code entityType} using the specified {@link Consumer updater} and return it in its new
	 * state.
	 */
	<OE extends Entity> OE update(Class<OE> entityType, String id, Consumer<OE> updater) {
		OE mapping = activeObjects.get(entityType, id);
		updater.accept(mapping);
		mapping.save();
		return mapping;
	}

	/**
	 * Delete a single {@link Entity} of the specified {@code entityType}, with the specified {@code id}
	 */
	<OE extends Entity> void delete(Class<OE> entityType, String id) {
		OE mapping = activeObjects.get(entityType, id);
		if (mapping != null) {
			deleteEntity(mapping);
		}
	}

	/**
	 * Delete all the {@link Entity} of the specified {@code entityType}, that match the specified {@link Query};
	 */
	<OE extends Entity> void delete(Class<OE> entityType, Query query) {
		deleteEntity(activeObjects.find(entityType, query));
	}

	void deleteOne(String id) {
		getOne(id).ifPresent(this::delete);
	}

	/**
	 * Delete all {@link Entity Entities}, and returns them, that match the specified {@code query}
	 */
	void delete(Query query) {
		stream(activeObjects.find(entityClass, query)).forEach(this::delete);
	}

	abstract void delete(E entity);

	/**
	 * Delete {@link Entity Entities} using a where criteria statement
	 */
	void deleteWithSQL(String criteria, Object... parameters) {
		activeObjects.deleteWithSQL(entityClass, criteria, parameters);
	}

	void deleteEntity(Entity... entity) {
		if (entity != null) {
			stream(entity).filter(Objects::nonNull)
			              .peek(object -> logger
					              .debug("Deleting entity {} with ID {}", object.getEntityType().getSimpleName(), object.getID()))
			              .forEach(activeObjects::delete);
		}
	}
}
