/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import java.net.URI;

import org.marvelution.jji.data.access.NullCharacterRemovingHashMap;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Result;
import org.marvelution.jji.model.SiteType;
import org.marvelution.jji.synctoken.utils.SharedSecretGenerator;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.marvelution.jji.data.access.EntityHelper.newId;
import static org.marvelution.jji.utils.KeyExtractor.extractProjectKeyFromIssueKey;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;
import static net.java.ao.Query.select;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.stripEnd;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
@SuppressWarnings("deprecation")
public class To_1_ModelV2Migrator implements ActiveObjectsUpgradeTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(To_1_ModelV2Migrator.class);

	@Override
	public ModelVersion getModelVersion() {
		return ModelVersion.valueOf("1");
	}

	@Override
	@SuppressWarnings("unchecked")
	public void upgrade(ModelVersion modelVersion, ActiveObjects activeObjects) {
		LOGGER.info("Upgrading AO model from {} to {}", modelVersion, getModelVersion());

		activeObjects.migrate(
				// Model v1
				org.marvelution.jji.data.access.model.v1.BuildMapping.class,
				org.marvelution.jji.data.access.model.v1.IssueMapping.class,
				org.marvelution.jji.data.access.model.v1.JobMapping.class,
				org.marvelution.jji.data.access.model.v1.SiteMapping.class,
				org.marvelution.jji.data.access.model.v1.TestResultsMapping.class,
				// Model v2
				BuildEntity.class,
				ChangeSetEntity.class,
				IssueLinkEntity.class,
				JobEntity.class,
				SiteEntity.class,
				TestResultsEntity.class
		);

		migrateSites(activeObjects);
	}

	private void migrateSites(ActiveObjects activeObjects) {
		for (org.marvelution.jji.data.access.model.v1.SiteMapping oldMapping : activeObjects.find(
				org.marvelution.jji.data.access.model.v1.SiteMapping.class)) {
			String siteName = ofNullable(oldMapping.getName())
					.orElse(format("%s at %s", oldMapping.getSiteType(), oldMapping.getRpcUrl()));
			try {
				// map old to new
				String newSiteId = newId();
				NullCharacterRemovingHashMap newMapping = new NullCharacterRemovingHashMap();
				newMapping.put(SiteEntity.ID, newSiteId);
				newMapping.put(SiteEntity.NAME, siteName);
				newMapping.put(SiteEntity.SHARED_SECRET, SharedSecretGenerator.generate());
				newMapping.put(SiteEntity.SITE_TYPE, SiteType.valueOf(oldMapping.getSiteType()));
				newMapping.put(SiteEntity.RPC_URL, URI.create(stripEnd(oldMapping.getRpcUrl(), "/") + "/"));
				if (oldMapping.getDisplayUrl() != null) {
					newMapping.put(SiteEntity.DISPLAY_URL, URI.create(stripEnd(oldMapping.getDisplayUrl(), "/") + "/"));
				}
				newMapping.put(SiteEntity.USER, oldMapping.getUser());
				newMapping.put(SiteEntity.USER_TOKEN, oldMapping.getUserToken());
				newMapping.put(SiteEntity.USE_CRUMBS, oldMapping.isUseCrumbs());
				newMapping.put(SiteEntity.AUTO_LINK_NEW_JOBS, oldMapping.isAutoLink());
				newMapping.put(SiteEntity.JENKINS_PLUGIN_INSTALLED, oldMapping.isSupportsBackLink());
				// store new mapping
				activeObjects.create(SiteEntity.class, newMapping);
				LOGGER.info("Migrated site {} with old id {} to {}", siteName, oldMapping.getID(), newSiteId);
				// migrate jobs
				migrateSiteJobs(activeObjects, oldMapping.getID(), newSiteId);
			} catch (Exception e) {
				LOGGER.error("Error during JJI site {} data migration: {}", siteName, e.getMessage(), e);
			}
		}
	}

	private void migrateSiteJobs(ActiveObjects activeObjects, int oldSiteId, String newSiteId) {
		for (org.marvelution.jji.data.access.model.v1.JobMapping oldMapping : activeObjects.find(
				org.marvelution.jji.data.access.model.v1.JobMapping.class,
				select().where(org.marvelution.jji.data.access.model.v1.JobMapping.SITE_ID + " = ?", oldSiteId))) {
			try {
				// map old to new
				String newJobId = newId();
				NullCharacterRemovingHashMap newMapping = new NullCharacterRemovingHashMap();
				newMapping.put(JobEntity.ID, newJobId);
				newMapping.put(JobEntity.SITE_ID, newSiteId);
				newMapping.put(JobEntity.NAME, oldMapping.getName());
				newMapping.put(JobEntity.URL_NAME, oldMapping.getUrlName());
				newMapping.put(JobEntity.DISPLAY_NAME, new Job().setUrlName(oldMapping.getUrlName())
				                                                .setDisplayName(oldMapping.getDisplayName())
				                                                .getDisplayNameOrNull());
				newMapping.put(JobEntity.LAST_BUILD, oldMapping.getLastBuild());
				newMapping.put(JobEntity.LINKED, oldMapping.isLinked());
				newMapping.put(JobEntity.DELETED, oldMapping.isDeleted());
				// store new mapping
				activeObjects.create(JobEntity.class, newMapping);
				LOGGER.info("Migrated job {} with old id {} to {}", oldMapping.getName(), oldMapping.getID(), newSiteId);
				// migrate builds
				migrateJobBuilds(activeObjects, oldMapping.getID(), newJobId);
			} catch (Exception e) {
				LOGGER.error("Error during JJI job {} data migration: {}", oldMapping.getName(), e.getMessage(), e);
			}
		}
	}

	private void migrateJobBuilds(ActiveObjects activeObjects, int oldJobId, String newJobId) {
		for (org.marvelution.jji.data.access.model.v1.BuildMapping oldMapping : activeObjects.find(
				org.marvelution.jji.data.access.model.v1.BuildMapping.class,
				select().where(org.marvelution.jji.data.access.model.v1.BuildMapping.JOB_ID + " = ?", oldJobId))) {
			try {
				// map old to new
				String newBuildId = newId();
				NullCharacterRemovingHashMap newMapping = new NullCharacterRemovingHashMap();
				newMapping.put(BuildEntity.ID, newBuildId);
				newMapping.put(BuildEntity.JOB_ID, newJobId);
				newMapping.put(BuildEntity.BUILD_NUMBER, oldMapping.getBuildNumber());
				newMapping.put(BuildEntity.DISPLAY_NAME, oldMapping.getDisplayName());
				newMapping.put(BuildEntity.CAUSE, oldMapping.getCause());
				newMapping.put(BuildEntity.DURATION, oldMapping.getDuration());
				newMapping.put(BuildEntity.TIME_STAMP, oldMapping.getTimeStamp());
				newMapping.put(BuildEntity.RESULT, Result.fromString(oldMapping.getResult()));
				newMapping.put(BuildEntity.BUILT_ON, oldMapping.getBuiltOn());
				newMapping.put(BuildEntity.DELETED, oldMapping.isDeleted());
				// store new mapping
				activeObjects.create(BuildEntity.class, newMapping);
				// migrate other build data
				migrateBuildTestResults(activeObjects, oldMapping.getID(), newBuildId);
				migrateIssueLinks(activeObjects, oldMapping.getID(), newBuildId);
			} catch (Exception e) {
				LOGGER.error("Error during JJI build {} #{} data migration: {}", oldMapping.getJobId(), oldMapping.getBuildNumber(),
				             e.getMessage(), e);
			}
		}
	}

	private void migrateBuildTestResults(ActiveObjects activeObjects, int oldBuildId, String newBuildId) {
		org.marvelution.jji.data.access.model.v1.TestResultsMapping[] mappings = activeObjects.find(
				org.marvelution.jji.data.access.model.v1.TestResultsMapping.class,
				select().where(org.marvelution.jji.data.access.model.v1.TestResultsMapping.BUILD_ID + " = ?", oldBuildId));
		if (mappings != null && mappings.length == 1) {
			// map old to new
			NullCharacterRemovingHashMap newMapping = new NullCharacterRemovingHashMap();
			newMapping.put(TestResultsEntity.ID, newId());
			newMapping.put(TestResultsEntity.BUILD_ID, newBuildId);
			newMapping.put(TestResultsEntity.SKIPPED, mappings[0].getSkipped());
			newMapping.put(TestResultsEntity.FAILED, mappings[0].getFailed());
			newMapping.put(TestResultsEntity.TOTAL, mappings[0].getTotal());
			// store new mapping
			activeObjects.create(TestResultsEntity.class, newMapping);
		}
	}

	private void migrateIssueLinks(ActiveObjects activeObjects, int oldBuildId, String newBuildId) {
		for (org.marvelution.jji.data.access.model.v1.IssueMapping oldMapping : activeObjects.find(
				org.marvelution.jji.data.access.model.v1.IssueMapping.class,
				select().where(org.marvelution.jji.data.access.model.v1.IssueMapping.BUILD_ID + " = ?", oldBuildId))) {
			if (oldMapping.getIssueKey() != null) {
				String projectKey = ofNullable(oldMapping.getProjectKey()).orElse(extractProjectKeyFromIssueKey(oldMapping.getIssueKey()));
				if (isBlank(projectKey)) {
					LOGGER.warn("Unable to extract project key from issue key {}", oldMapping.getIssueKey());
				}
				// map old to new
				NullCharacterRemovingHashMap newMapping = new NullCharacterRemovingHashMap();
				newMapping.put(IssueLinkEntity.ID, newId());
				newMapping.put(IssueLinkEntity.BUILD_ID, newBuildId);
				newMapping.put(IssueLinkEntity.PROJECT_KEY, projectKey);
				newMapping.put(IssueLinkEntity.ISSUE_KEY, oldMapping.getIssueKey());
				// store new mapping
				activeObjects.create(IssueLinkEntity.class, newMapping);
			}
		}
	}

}
