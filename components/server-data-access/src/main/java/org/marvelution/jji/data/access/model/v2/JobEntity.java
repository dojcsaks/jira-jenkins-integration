/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import javax.annotation.Nullable;

import net.java.ao.OneToMany;
import net.java.ao.Preload;
import net.java.ao.schema.Default;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import static net.java.ao.schema.StringLength.MAX_LENGTH;
import static net.java.ao.schema.StringLength.UNLIMITED;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
@Preload
@Table("Jobs")
public interface JobEntity extends Entity {

	String SITE_ID = "SITE_ID";
	String NAME = "NAME";
	String URL_NAME = "URL_NAME";
	String DISPLAY_NAME = "DISPLAY_NAME";
	String DESCRIPTION = "DESCRIPTION";
	String LAST_BUILD = "LAST_BUILD";
	String OLDEST_BUILD = "OLDEST_BUILD";
	String LINKED = "LINKED";
	String DELETED = "DELETED";

	@NotNull
	SiteEntity getSite();

	void setSite(SiteEntity site);

	@NotNull
	String getName();

	void setName(String name);

	@Nullable
	String getUrlName();

	void setUrlName(@Nullable String urlName);

	@Nullable
	@StringLength(MAX_LENGTH)
	String getDisplayName();

	void setDisplayName(@Nullable String displayName);

	@Nullable
	@StringLength(UNLIMITED)
	String getDescription();

	void setDescription(@Nullable String description);

	@Default("0")
	int getLastBuild();

	void setLastBuild(int lastBuild);

	@Default("-1")
	int getOldestBuild();

	void setOldestBuild(int oldestBuild);

	boolean isLinked();

	void setLinked(boolean linked);

	boolean isDeleted();

	void setDeleted(boolean deleted);

	@OneToMany(reverse = "getJob")
	BuildEntity[] getBuilds();

}
