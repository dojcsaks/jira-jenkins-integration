/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.util.Optional;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.access.api.IssueDAO;
import org.marvelution.jji.data.services.api.BuildLinkService;
import org.marvelution.jji.data.services.api.BuildService;
import org.marvelution.jji.data.services.api.IssueReferenceProvider;
import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.events.BuildIssueLinksUpdatedEvent;
import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.IssueReference;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.ProjectDeletedEvent;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toSet;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
@ExportAsDevService(BuildLinkService.class)
public class DefaultBuildLinkService extends BaseBuildLinkService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBuildLinkService.class);
	private final EventPublisher eventPublisher;

	@Inject
	public DefaultBuildLinkService(IssueDAO issueDAO, IssueReferenceProvider issueReferenceProvider, JobService jobService,
	                               BuildService buildService, @ComponentImport EventPublisher eventPublisher) {
		super(issueDAO, issueReferenceProvider, jobService, buildService);
		this.eventPublisher = eventPublisher;
	}

	@PostConstruct
	public void registerWithPublisher() throws Exception {
		eventPublisher.register(this);
	}

	@PreDestroy
	public void unregisterWithPublisher() throws Exception {
		eventPublisher.unregister(this);
	}

	@Override
	public Optional<IssueReference> link(Build build, String issueKey) {
		Optional<IssueReference> reference = super.link(build, issueKey);
		reference.ifPresent(ref -> eventPublisher.publish(new BuildIssueLinksUpdatedEvent(build.getId(), singleton(ref.getIssueKey()))));
		return reference;
	}

	@Override
	public Set<IssueReference> relink(Build build, Set<String> issueKeys) {
		Set<IssueReference> references = super.relink(build, issueKeys);
		eventPublisher.publish(
				new BuildIssueLinksUpdatedEvent(build.getId(), references.stream().map(IssueReference::getIssueKey).collect(toSet())));
		return references;
	}

	@EventListener
	public void onIssueEvent(IssueEvent event) {
		if (EventType.ISSUE_DELETED_ID.equals(event.getEventTypeId())) {
			LOGGER.info("Updating build-to-issue index for deleted issue {}", event.getIssue().getKey());
			unlinkForIssueKey(event.getIssue().getKey());
		}
	}
	
	@EventListener
	public void onProjectDeleted(ProjectDeletedEvent event) {
		LOGGER.info("Updating build-to-project index for deleted project {}", event.getKey());
		unlinkForProjectKey(event.getKey());
	}
}
