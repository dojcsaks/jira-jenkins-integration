/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access.model.v2;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link ActiveObjectsUpgradeTask} to migrate the v2 model to remove the length constraint from the primary key, MSSQL doesn't allow
 * different column length between primary and foreign key relations and the ActiveObject module doesn't lookup any constraints on the
 * primary key when creating the foreign key column.
 *
 * @author Mark Rekveld
 * @since 3.0.1
 */
public class To_2_RemovedPrimaryKeyLengthConstraint implements ActiveObjectsUpgradeTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(To_2_RemovedPrimaryKeyLengthConstraint.class);

	@Override
	public ModelVersion getModelVersion() {
		return ModelVersion.valueOf("2");
	}

	@Override
	@SuppressWarnings("unchecked")
	public void upgrade(ModelVersion modelVersion, ActiveObjects activeObjects) {
		LOGGER.info("Upgrading AO model from {} to {}", modelVersion, getModelVersion());

		activeObjects.migrate(
				BuildEntity.class,
				ChangeSetEntity.class,
				IssueLinkEntity.class,
				JobEntity.class,
				SiteEntity.class,
				TestResultsEntity.class
		);
	}
}
