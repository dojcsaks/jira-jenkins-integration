/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jji.data.access.api.SiteDAO;
import org.marvelution.jji.data.access.model.v2.SiteEntity;
import org.marvelution.jji.model.Site;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static org.marvelution.jji.data.access.EntityHelper.deleteSite;
import static org.marvelution.jji.data.access.model.v2.SiteEntity.*;

import static java.util.stream.Collectors.toList;
import static net.java.ao.Query.select;

/**
 * Default {@link SiteDAO} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultSiteDAO extends AbstractModelDAO<Site, SiteEntity> implements SiteDAO {

	@Inject
	public DefaultSiteDAO(@ComponentImport ActiveObjects activeObjects) {
		super(activeObjects, SiteEntity.class, TransformerHelper::entityToSite);
	}

	@Override
	public List<Site> getAll() {
		return find(select())
				.map(transformer)
				.collect(toList());
	}

	@Override
	public Optional<Site> findByName(String name) {
		return findOne(select().where(NAME + " = ?", name))
				.map(transformer);
	}

	@Override
	public Optional<Site> findByRpcUrl(URI rpcUri) {
		return findOne(select().where(RPC_URL + " = ?", rpcUri))
				.map(transformer);
	}

	@Override
	NullCharacterRemovingHashMap toInsertMap(Site site) {
		NullCharacterRemovingHashMap entity = new NullCharacterRemovingHashMap();
		entity.put(NAME, site.getName());
		entity.put(SHARED_SECRET, site.getSharedSecret());
		entity.put(SITE_TYPE, site.getType());
		entity.put(RPC_URL, site.getRpcUrl());
		entity.put(DISPLAY_URL, site.getDisplayUrlOrNull());
		entity.put(USER, site.getUser());
		entity.put(USER_TOKEN, site.getToken());
		entity.put(AUTO_LINK_NEW_JOBS, site.isAutoLinkNewJobs());
		entity.put(JENKINS_PLUGIN_INSTALLED, site.isJenkinsPluginInstalled());
		entity.put(USE_CRUMBS, site.isUseCrumbs());
		return entity;
	}

	@Override
	void updateEntity(SiteEntity entity, Site site) {
		entity.setName(site.getName());
		entity.setSiteType(site.getType());
		entity.setRpcUrl(site.getRpcUrl());
		entity.setDisplayUrl(site.getDisplayUrlOrNull());
		entity.setUser(site.getUser());
		entity.setUserToken(site.getToken());
		entity.setJenkinsPluginInstalled(site.isJenkinsPluginInstalled());
		entity.setAutoLinkNewJobs(site.isAutoLinkNewJobs());
		entity.setUseCrumbs(site.isUseCrumbs());
	}

	@Override
	void delete(SiteEntity entity) {
		deleteSite(entity, this);
	}
}
