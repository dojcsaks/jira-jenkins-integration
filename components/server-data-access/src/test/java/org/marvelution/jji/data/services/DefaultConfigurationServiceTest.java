/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import java.net.URI;

import org.marvelution.jji.data.services.api.TextResolver;
import org.marvelution.jji.data.services.api.exceptions.ValidationException;
import org.marvelution.jji.model.Configuration;
import org.marvelution.testing.TestSupport;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.testresources.pluginsettings.MockPluginSettings;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.marvelution.jji.data.services.DefaultConfigurationService.JIRA_BASE_RPC_URL;
import static org.marvelution.jji.data.services.DefaultConfigurationService.MAX_BUILDS_PER_PAGE;
import static org.marvelution.jji.data.services.DefaultConfigurationService.USE_JOB_LATEST_BUILD_WHEN_INDEXING;
import static org.marvelution.jji.data.services.api.ConfigurationService.DEFAULT_MAX_BUILDS_PER_PAGE;
import static org.marvelution.jji.model.Matchers.errorForField;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link DefaultConfigurationService}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class DefaultConfigurationServiceTest extends TestSupport {

	@Mock
	private ApplicationProperties applicationProperties;
	@Mock
	private PluginSettingsFactory pluginSettingsFactory;
	private PluginSettings pluginSettings;
	@Mock
	private TextResolver textResolver;
	private DefaultConfigurationService configurationService;

	@Before
	public void setUp() throws Exception {
		pluginSettings = new MockPluginSettings();
		when(pluginSettingsFactory.createGlobalSettings()).thenReturn(pluginSettings);
		when(applicationProperties.getDisplayName()).thenReturn("JIRA Software");
		when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn("http://jira.example.com");
		configurationService = new DefaultConfigurationService(textResolver, applicationProperties, pluginSettingsFactory);
	}

	@Test
	public void testGetJIRAInstanceName() throws Exception {
		assertThat(configurationService.getJIRAInstanceName(), is("JIRA Software"));
	}

	@Test
	public void testGetJIRABaseUrl() throws Exception {
		assertThat(configurationService.getJIRABaseUrl(), is(URI.create("http://jira.example.com")));
	}

	@Test
	public void testGetJIRABaseRpcUrl() throws Exception {
		assertThat(configurationService.getJIRABaseRpcUrl(), is(URI.create("http://jira.example.com")));
	}

	@Test
	public void testGetJIRABaseRpcUrl_Overridden() throws Exception {
		pluginSettings.put(JIRA_BASE_RPC_URL, "http://jira.local");

		assertThat(configurationService.getJIRABaseRpcUrl(), is(URI.create("http://jira.local")));
	}

	@Test
	public void testGetMaximumBuildsPerPage() throws Exception {
		pluginSettings.put(MAX_BUILDS_PER_PAGE, "1");

		assertThat(configurationService.getMaximumBuildsPerPage(), is(1));
	}

	@Test
	public void testGetMaximumBuildsPerPage_Default() throws Exception {
		assertThat(configurationService.getMaximumBuildsPerPage(), is(DEFAULT_MAX_BUILDS_PER_PAGE));
	}

	@Test
	public void testGetMaximumBuildsPerPage_InvalidNumber() throws Exception {
		pluginSettings.put(MAX_BUILDS_PER_PAGE, "a1");

		assertThat(configurationService.getMaximumBuildsPerPage(), is(DEFAULT_MAX_BUILDS_PER_PAGE));
	}

	@Test
	public void testUseJobLatestBuildResultInIndexes_Enabled() throws Exception {
		pluginSettings.put(USE_JOB_LATEST_BUILD_WHEN_INDEXING, "true");

		assertThat(configurationService.useJobLatestBuildResultInIndexes(), is(true));
	}

	@Test
	public void testUseJobLatestBuildResultInIndexes_Disabled() throws Exception {
		pluginSettings.put(USE_JOB_LATEST_BUILD_WHEN_INDEXING, "false");

		assertThat(configurationService.useJobLatestBuildResultInIndexes(), is(false));
	}

	@Test
	public void testUseJobLatestBuildResultInIndexes_InvalidBooleanDefaultToDisabled() throws Exception {
		pluginSettings.put(USE_JOB_LATEST_BUILD_WHEN_INDEXING, "I'm not a boolean");

		assertThat(configurationService.useJobLatestBuildResultInIndexes(), is(false));
	}

	@Test
	public void testGetConfiguration_Defaults() throws Exception {
		Configuration configuration = configurationService.getConfiguration();

		assertThat(configuration.getJIRAInstanceName(), is("JIRA Software"));
		assertThat(configuration.getJIRABaseUrl(), is(URI.create("http://jira.example.com")));
		assertThat(configuration.getJIRABaseRpcUrl(), is(URI.create("http://jira.example.com")));
		assertThat(configuration.getMaxBuildsPerPage(), is(DEFAULT_MAX_BUILDS_PER_PAGE));
		assertThat(configuration.isUseJobLatestBuildWhenIndexing(), is(false));
	}

	@Test
	public void testGetConfiguration_Customized() throws Exception {
		pluginSettings.put(JIRA_BASE_RPC_URL, "http://jira.local");
		pluginSettings.put(MAX_BUILDS_PER_PAGE, "1000");
		pluginSettings.put(USE_JOB_LATEST_BUILD_WHEN_INDEXING, "true");

		Configuration configuration = configurationService.getConfiguration();

		assertThat(configuration.getJIRAInstanceName(), is("JIRA Software"));
		assertThat(configuration.getJIRABaseUrl(), is(URI.create("http://jira.example.com")));
		assertThat(configuration.getJIRABaseRpcUrl(), is(URI.create("http://jira.local")));
		assertThat(configuration.getMaxBuildsPerPage(), is(1000));
		assertThat(configuration.isUseJobLatestBuildWhenIndexing(), is(true));
	}

	@Test
	public void testSaveConfiguration_BlankConfiguration() throws Exception {
		configurationService.saveConfiguration(new Configuration());

		assertThat(pluginSettings.get(JIRA_BASE_RPC_URL), nullValue());
		assertThat(pluginSettings.get(MAX_BUILDS_PER_PAGE), nullValue());
		assertThat(pluginSettings.get(USE_JOB_LATEST_BUILD_WHEN_INDEXING), is("false"));
	}

	@Test
	public void testSaveConfiguration_DefaultConfiguration() throws Exception {
		configurationService.saveConfiguration(configurationService.getConfiguration());

		assertThat(pluginSettings.get(JIRA_BASE_RPC_URL), nullValue());
		assertThat(pluginSettings.get(MAX_BUILDS_PER_PAGE), is("100"));
		assertThat(pluginSettings.get(USE_JOB_LATEST_BUILD_WHEN_INDEXING), is("false"));
	}

	@Test
	public void testSaveConfiguration_CustomizedConfiguration() throws Exception {
		configurationService.saveConfiguration(new Configuration()
				                                       .setMaxBuildsPerPage(1000)
				                                       .setJIRABaseRpcUrl(URI.create("http://jira.local"))
				                                       .setUseJobLatestBuildWhenIndexing(true));

		assertThat(pluginSettings.get(JIRA_BASE_RPC_URL), is("http://jira.local"));
		assertThat(pluginSettings.get(MAX_BUILDS_PER_PAGE), is("1000"));
		assertThat(pluginSettings.get(USE_JOB_LATEST_BUILD_WHEN_INDEXING), is("true"));
	}

	@Test
	public void testSaveConfiguration_DontSaveBaseRpcUrlIfEqualToBaseUrl() throws Exception {
		configurationService.saveConfiguration(new Configuration()
				                                       .setJIRABaseRpcUrl(URI.create("http://jira.example.com")));

		assertThat(pluginSettings.get(JIRA_BASE_RPC_URL), nullValue());
		assertThat(pluginSettings.get(MAX_BUILDS_PER_PAGE), nullValue());
		assertThat(pluginSettings.get(USE_JOB_LATEST_BUILD_WHEN_INDEXING), is("false"));
	}

	@Test
	public void testSaveConfiguration_InvalidBaseRpcUrl() throws Exception {
		try {
			configurationService.saveConfiguration(new Configuration()
					                                       .setJIRABaseRpcUrl(URI.create("jira.example.com")));
			fail("Expected ValidationException");
		} catch (ValidationException e) {
			assertThat(e.getMessages().getErrors(), hasSize(1));
			assertThat(e.getMessages().getErrors(), hasItem(errorForField("jiraBaseRpcUrl")));
		}
	}

	@Test
	public void testSaveConfiguration_UnlimitedBuildsPerPage() throws Exception {
		configurationService.saveConfiguration(new Configuration()
				                                       .setMaxBuildsPerPage(-1));

		assertThat(pluginSettings.get(JIRA_BASE_RPC_URL), nullValue());
		assertThat(pluginSettings.get(MAX_BUILDS_PER_PAGE), is("-1"));
		assertThat(pluginSettings.get(USE_JOB_LATEST_BUILD_WHEN_INDEXING), is("false"));
	}

	@Test
	public void testSaveConfiguration_NegativeLimit() throws Exception {
		configurationService.saveConfiguration(new Configuration()
				                                       .setMaxBuildsPerPage(-12));

		assertThat(pluginSettings.get(JIRA_BASE_RPC_URL), nullValue());
		assertThat(pluginSettings.get(MAX_BUILDS_PER_PAGE), nullValue());
		assertThat(pluginSettings.get(USE_JOB_LATEST_BUILD_WHEN_INDEXING), is("false"));
	}
}
