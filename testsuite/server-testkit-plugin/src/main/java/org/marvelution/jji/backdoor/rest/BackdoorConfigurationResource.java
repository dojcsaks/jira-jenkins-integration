/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.ConfigurationService;
import org.marvelution.jji.model.Configuration;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

/**
 * @author Mark Rekveld
 * @since 2.3.0
 */
@AnonymousAllowed
@Path("configuration")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorConfigurationResource {

	private final ConfigurationService configurationService;

	public BackdoorConfigurationResource(@ComponentImport ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	@GET
	public Configuration getConfigurationService() {
		return configurationService.getConfiguration();
	}

	@POST
	public Configuration savePluginConfiguration(Configuration configuration) {
		configurationService.saveConfiguration(configuration);
		return configurationService.getConfiguration();
	}

	/**
	 * @since 3.0.0
	 */
	@DELETE
	public void clearConfiguration() {
		Configuration configuration = configurationService.getConfiguration();
		configuration.setMaxBuildsPerPage(ConfigurationService.DEFAULT_MAX_BUILDS_PER_PAGE)
		             .setUseJobLatestBuildWhenIndexing(false)
		             .setJIRABaseRpcUrl(configuration.getJIRABaseUrl());
		configurationService.saveConfiguration(configuration);
	}
}
