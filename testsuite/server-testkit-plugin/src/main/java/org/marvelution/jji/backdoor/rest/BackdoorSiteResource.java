/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor.rest;

import java.util.List;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.data.services.api.SiteService;
import org.marvelution.jji.model.Site;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
@AnonymousAllowed
@Path("site")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorSiteResource {

	private final SiteService siteService;
	private final JobService jobService;

	public BackdoorSiteResource(@ComponentImport SiteService siteService, @ComponentImport JobService jobService) {
		this.siteService = siteService;
		this.jobService = jobService;
	}

	@GET
	public List<Site> getAll() {
		return siteService.getAll(true);
	}

	@POST
	public Site save(Site site) {
		return siteService.save(site);
	}

	@DELETE
	public void clear() {
		siteService.getAll().forEach(siteService::delete);
	}

	@GET
	@Path("{siteId}")
	@Nullable
	public Site get(@PathParam("siteId") String siteId, @QueryParam("includeJobs") boolean includeJobs) {
		return siteService.get(siteId)
		                  .map(site -> {
			                  if (includeJobs) {
				                  site.setJobs(jobService.getAllBySite(site, true));
			                  }
			                  return site;
		                  })
		                  .orElse(null);
	}

	@GET
	@Path("{siteId}/sharedSecret")
	@Nullable
	public String getSharedSecret(@PathParam("siteId") String siteId) {
		return siteService.get(siteId).map(Site::getSharedSecret).orElse(null);
	}

	@DELETE
	@Path("{siteId}")
	public void delete(@PathParam("siteId") String siteId) {
		siteService.get(siteId).ifPresent(siteService::delete);
	}

	@PUT
	@Path("{siteId}/sync")
	public void synchronize(@PathParam("siteId") String siteId) {
		siteService.get(siteId).ifPresent(siteService::synchronize);
	}
}
