/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.backdoor.rest;

import java.util.List;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.marvelution.jji.data.services.api.JobService;
import org.marvelution.jji.model.Job;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
@AnonymousAllowed
@Path("job")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorJobResource {

	private final JobService jobService;

	public BackdoorJobResource(@ComponentImport JobService jobService) {
		this.jobService = jobService;
	}

	@GET
	public List<Job> getAll() {
		return jobService.getAll(true);
	}

	@POST
	public Job save(Job job) {
		return jobService.save(job);
	}

	@Nullable
	@GET
	@Path("{jobId}")
	public Job get(@PathParam("jobId") String jobId) {
		return jobService.get(jobId).orElse(null);
	}

	@DELETE
	@Path("{jobId}")
	public void delete(@PathParam("jobId") String jobId) {
		jobService.get(jobId).ifPresent(jobService::delete);
	}

	@PUT
	@Path("{jobId}/sync")
	public void synchronize(@PathParam("jobId") String jobId) {
		jobService.get(jobId).ifPresent(jobService::synchronize);
	}
}
