The maven-jira-plugin (internally maven-amps-plugin) checks for the existence on the `it` directory within the `target/test-classes`
directory.
This readme only exists to force the creation of the `it` directory so the execution of the `integration-test` goal continues.
