/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.services;

import javax.inject.Inject;

import org.marvelution.jji.data.access.ActiveObjectTransactionTestRuleFactory;
import org.marvelution.jji.data.access.DataAccessModule;
import org.marvelution.jji.data.access.ModelDatabaseUpdater;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.Status;
import org.marvelution.jji.utils.UrlGenerator;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.testresources.net.MockRequest;
import com.atlassian.sal.testresources.net.MockResponse;
import com.google.inject.Binder;
import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.Jdbc;
import org.junit.Rule;
import org.junit.rules.TestRule;

import static org.marvelution.jji.utils.UrlGenerator.Mode.SYNC;

/**
 * Tests for {@link DefaultSiteService}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Jdbc
@Data(ModelDatabaseUpdater.class)
public class DefaultSiteServiceIT extends AbstractBaseSiteServiceIT<DefaultSiteService> {

	private EntityManager entityManager;
	@Rule
	public TestRule transactionRule = ActiveObjectTransactionTestRuleFactory.create(this);
	@Inject
	private NonMarshallingMockRequestFactory requestFactory;

	@Override
	public void configure(Binder binder) {
		binder.install(new DataAccessModule(() -> entityManager));
		binder.install(new DataServicesModule());
	}

	@Override
	protected void setupSiteClient(Site site, Status status, boolean pluginInstalled) throws Exception {
		setupStatusRequest(site, status);
		setupPluginRequest(site, pluginInstalled);
	}

	private void setupStatusRequest(Site site, Status status) {
		String url = UrlGenerator.mode(SYNC).site(site).api().url().toASCIIString();
		MockRequest request = new MockRequest(Request.MethodType.GET, url);
		if (status == Status.ONLINE || status == Status.NOT_ACCESSIBLE) {
			MockResponse response = new MockResponse();
			response.setSuccessful(true);
			response.setStatusCode(status == Status.ONLINE ? 200 : 401);
			response.setStatusText(status.name());
			request.setResponse(response);
		}
		requestFactory.addRequest(url, request);
	}

	private void setupPluginRequest(Site site, boolean pluginInstalled) {
		String url = UrlGenerator.mode(SYNC).site(site).path("plugin/jenkins-jira-plugin/ping.html").url().toASCIIString();
		MockRequest request = new MockRequest(Request.MethodType.GET, url);
		MockResponse response = new MockResponse();
		response.setSuccessful(pluginInstalled);
		response.setStatusCode(pluginInstalled ? 200 : 404);
		request.setResponse(response);
		requestFactory.addRequest(url, request);
	}
}
