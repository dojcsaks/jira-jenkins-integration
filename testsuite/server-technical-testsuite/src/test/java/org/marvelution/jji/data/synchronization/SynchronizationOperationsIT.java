/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.synchronization;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.marvelution.jji.model.Build;
import org.marvelution.jji.model.ChangeSet;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Result;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;
import org.marvelution.jji.model.TestResults;
import org.marvelution.jji.rest.AbstractResourceTest;
import org.marvelution.testing.wiremock.WireMockRule;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Project;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;

import static org.marvelution.jji.model.Matchers.changeSetEqualTo;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Mark Rekveld
 * @since 2.1.0
 */
public class SynchronizationOperationsIT extends AbstractResourceTest {

	@Rule
	public WireMockRule jenkins = new WireMockRule();

	@After
	public void tearDown() throws Exception {
		backdoor.sites().clearSites();
	}

	private Site createAndAddSite(int expectedJobCount) {
		Site site = new Site();
		site.setType(SiteType.JENKINS);
		site.setName(testName.getMethodName());
		site.setRpcUrl(jenkins.serverUri());
		site.setAutoLinkNewJobs(true);
		Site added = siteResource().entity(site, APPLICATION_JSON_TYPE).post(Site.class);
		await().atMost(30, TimeUnit.SECONDS)
		       .pollInterval(1, TimeUnit.SECONDS)
		       .until(() -> backdoor.sites().getSite(added.getId()).getJobs().size() == expectedJobCount);
		return backdoor.sites().getSite(added.getId());
	}

	@Test
	public void testFolderJobSynchronization() throws Exception {
		Site site = createAndAddSite(5);
		assertThat(site.isJenkinsPluginInstalled(), is(false));
		assertThat(site.isUseCrumbs(), is(false));
		assertThat(site.getJobs(), hasSize(5));
		assertThat(site.getJobs().stream().filter(Job::isLinked).count(), is(5L));
		site.getJobs().stream()
		    .filter(job -> job.getName().equals("Cave"))
		    .forEach(job -> await(job.getUrlName()).atMost(30, TimeUnit.SECONDS)
		                                           .pollInterval(1, TimeUnit.SECONDS)
		                                           .until(() -> !backdoor.jobs().getJob(job.getId()).isDeleted()));
		site.getJobs().parallelStream()
		    .filter(job -> !job.getName().equals("Cave"))
		    .forEach(job -> await(job.getUrlName()).atMost(30, TimeUnit.SECONDS)
		                                           .pollInterval(1, TimeUnit.SECONDS)
		                                           .until(() -> backdoor.jobs().getJob(job.getId()).isDeleted()));
	}

	@Test
	public void testBuildSynchronization() throws Exception {
		Project project = backdoor.generator().generateScrumProject(testName.getMethodName());
		IssueCreateResponse issue = backdoor.issues().createIssue(project.key, testName.getMethodName());
		assertThat(issue.key(), is("TBS-1"));
		Site site = createAndAddSite(1);
		assertThat(site.isJenkinsPluginInstalled(), is(false));
		assertThat(site.isUseCrumbs(), is(false));
		assertThat(site.getJobs(), hasSize(1));
		Job job = site.getJobs().get(0);
		assertThat(job, is(notNullValue()));
		assertThat(job.getName(), is("Cave"));
		assertThat(job.getDisplayName(), is("Cave"));
		assertThat(job.getUrlName(), is("Cave"));
		assertThat(job.isLinked(), is(true));
		assertThat(job.isDeleted(), is(false));
		Set<Build> builds = backdoor.builds().getBuilds(job);
		assertThat(builds, hasSize(1));
		Build build = builds.iterator().next();
		assertThat(build, is(notNullValue()));
		assertThat(build.getId(), is(notNullValue()));
		assertThat(build.getJob().getId(), is(job.getId()));
		assertThat(build.getNumber(), is(1));
		assertThat(build.getDisplayName(), is(nullValue()));
		assertThat(build.getDescription(), is("My build description"));
		assertThat(build.isDeleted(), is(false));
		assertThat(build.getCause(), is("Started by an SCM change"));
		assertThat(build.getResult(), is(Result.FAILURE));
		assertThat(build.getBuiltOn(), is("remote-slave-6"));
		assertThat(build.getDuration(), is(135329L));
		assertThat(build.getTimestamp(), is(1355405446078L));
		List<ChangeSet> changeSet = build.getChangeSet();
		assertThat(changeSet, hasSize(2));
		assertThat(changeSet, hasItem(changeSetEqualTo("9a0d506fa8982e41ed3340caeed4a605587b6aa1", "Update something TBS-1")));
		assertThat(changeSet, hasItem(changeSetEqualTo("9a0d506fa8982e41ed3340caeed4a605587b6aa2", "License headers")));
		TestResults testResults = build.getTestResults();
		assertThat(testResults, is(notNullValue()));
		assertThat(testResults.getSkipped(), is(40));
		assertThat(testResults.getFailed(), is(10));
		assertThat(testResults.getTotal(), is(3121));
		Set<String> relatedIssueKeys = backdoor.builds().getRelatedIssueKeys(build);
		assertThat(relatedIssueKeys, hasSize(1));
		assertThat(relatedIssueKeys, hasItem("TBS-1"));
	}

}
