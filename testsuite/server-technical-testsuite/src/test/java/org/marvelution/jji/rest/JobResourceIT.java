/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.net.URI;

import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;

import com.sun.jersey.api.client.ClientResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.marvelution.jji.rest.Matchers.status;

import static org.junit.Assert.assertThat;

/**
 * Integration Testcase for the job REST resource
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class JobResourceIT extends AbstractResourceTest {

	private Job job;

	@Before
	public void setUp() throws Exception {
		Site site = backdoor.sites().addSite(SiteType.JENKINS, "Jenkins", URI.create("http://localhost:8080"));
		job = backdoor.jobs().addJob(site, testName.getMethodName());
	}

	@After
	public void tearDown() throws Exception {
		backdoor.sites().clearSites();
	}

	@Test
	public void testRemoveJob() throws Exception {
		ClientResponse delete = jobResource(job).delete(ClientResponse.class);
		assertThat(delete, status(ClientResponse.Status.BAD_REQUEST));
		job.setDeleted(true);
		backdoor.jobs().saveJob(job);
		delete = jobResource(job).delete(ClientResponse.class);
		assertThat(delete, status(ClientResponse.Status.NO_CONTENT));
	}

}
