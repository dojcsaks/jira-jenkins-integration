/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.data.access;

import java.lang.annotation.Annotation;

import net.java.ao.schema.FieldNameConverter;
import net.java.ao.schema.IndexNameConverter;
import net.java.ao.schema.SequenceNameConverter;
import net.java.ao.schema.TableNameConverter;
import net.java.ao.schema.TriggerNameConverter;
import net.java.ao.schema.UniqueNameConverter;
import net.java.ao.test.converters.NameConverters;
import net.java.ao.test.jdbc.Jdbc;
import net.java.ao.test.jdbc.JdbcConfiguration;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import net.java.ao.test.lucene.WithIndex;

/**
 * Factory method so the {@link ActiveObjectTransactionTestRule} can be used without using the {@link ActiveObjectsJUnitRunner}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class ActiveObjectTransactionTestRuleFactory {

	/**
	 * Creates and returns the {@link ActiveObjectTransactionTestRule} for the specified {@code test}.
	 */
	public static ActiveObjectTransactionTestRule create(Object test) {
		Class<?> testClass = test.getClass();
		return new ActiveObjectTransactionTestRule(test, jdbcConfiguration(testClass), withIndex(testClass),
		                                           tableNameConverter(testClass), fieldNameConverter(testClass),
		                                           sequenceNameConverter(testClass), triggerNameConverter(testClass),
		                                           indexNameConverter(testClass), uniqueNameConverter(testClass));
	}

	private static boolean withIndex(Class<?> klass) {
		return klass.isAnnotationPresent(WithIndex.class);
	}

	private static TableNameConverter tableNameConverter(Class<?> klass) {
		return newInstance(getNameConvertersAnnotation(klass).table());
	}

	private static FieldNameConverter fieldNameConverter(Class<?> klass) {
		return newInstance(getNameConvertersAnnotation(klass).field());
	}

	private static SequenceNameConverter sequenceNameConverter(Class<?> klass) {
		return newInstance(getNameConvertersAnnotation(klass).sequence());
	}

	private static TriggerNameConverter triggerNameConverter(Class<?> klass) {
		return newInstance(getNameConvertersAnnotation(klass).trigger());
	}

	private static IndexNameConverter indexNameConverter(Class<?> klass) {
		return newInstance(getNameConvertersAnnotation(klass).index());
	}

	private static UniqueNameConverter uniqueNameConverter(Class<?> klass) {
		return newInstance(getNameConvertersAnnotation(klass).unique());
	}

	private static NameConverters getNameConvertersAnnotation(Class<?> klass) {
		return getAnnotation(klass, NameConverters.class);
	}

	private static <A extends Annotation> A getAnnotation(Class<?> klass, Class<A> annotationClass) {
		if (klass.isAnnotationPresent(annotationClass)) {
			return klass.getAnnotation(annotationClass);
		}
		return Annotated.class.getAnnotation(annotationClass);
	}

	private static JdbcConfiguration jdbcConfiguration(Class<?> klass) {
		return newInstance(getAnnotation(klass, Jdbc.class).value());
	}

	private static <T> T newInstance(Class<T> type) {
		try {
			return type.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	@NameConverters
	@Jdbc
	private static final class Annotated {
	}
}
