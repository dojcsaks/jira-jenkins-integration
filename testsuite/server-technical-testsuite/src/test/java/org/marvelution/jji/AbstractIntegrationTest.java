/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji;

import org.marvelution.testing.TestSupport;
import org.marvelution.jji.testkit.backdoor.Backdoor;

import org.junit.BeforeClass;

/**
 * Base Integration Test
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
public abstract class AbstractIntegrationTest extends TestSupport {

	protected static final String ANONYMOUS = "anonymous";
	protected static final String USER = "user";
	protected static final String ADMIN = "admin";

	protected static Backdoor backdoor = new Backdoor();

	@BeforeClass
	public static void setupUsers() throws Exception {
		if (!backdoor.usersAndGroups().userExists(USER)) {
			backdoor.usersAndGroups().addUser(USER);
			backdoor.usersAndGroups().addUserToGroup(USER, "jira-software-users");
		}
	}
}
