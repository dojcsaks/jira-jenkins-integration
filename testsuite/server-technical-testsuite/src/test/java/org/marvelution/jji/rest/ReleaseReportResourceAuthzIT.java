/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.sun.jersey.api.client.ClientResponse;
import org.junit.Before;
import org.junit.Test;

/**
 * Integration Authz Testcase for the release report REST resource.
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
public class ReleaseReportResourceAuthzIT extends AbstractResourceAuthzTest {

	private IssueCreateResponse issue;

	@Before
	public void setUp() throws Exception {
		Project project = backdoor.generator().generateScrumProject(testName.getMethodName());
		issue = backdoor.issues().createIssue(project.key, "Authz Test: " + testName.getMethodName());
	}

	@Test
	public void testBuildInformation_Anonymous() throws Exception {
		testAuthzGet(releaseReportResource().path("build-info/" + issue.key), anonymous(ClientResponse.Status.UNAUTHORIZED));
	}

	@Test
	public void testBuildInformation_AuthenticatedUser() throws Exception {
		testAuthzGet(releaseReportResource().path("build-info/" + issue.key), authenticatedUser(ClientResponse.Status.OK));
	}

	@Test
	public void testBuildInformation_Administrator() throws Exception {
		testAuthzGet(releaseReportResource().path("build-info/" + issue.key), administrator(ClientResponse.Status.OK));
	}

}
