/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.ClientResponse;

import static org.apache.commons.lang3.StringUtils.capitalize;

/**
 * {@link BackdoorControl} for generating data in JIRA.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class GeneratorControl extends BackdoorControl<GeneratorControl> {

	private static final String GREENHOPPER_ADDON_KEY = "com.pyxis.greenhopper.jira";
	private static final String SCRUM_SOFTWARE_PROJECT_TEMPLATE_KEY = GREENHOPPER_ADDON_KEY + ":gh-scrum-template";
	private static final String KANBAN_SOFTWARE_PROJECT_TEMPLATE_KEY = GREENHOPPER_ADDON_KEY + ":gh-kanban-template";
	private static final String BASIC_SOFTWARE_PROJECT_TEMPLATE_KEY = GREENHOPPER_ADDON_KEY + ":basic-software-development-template";
	private static final String PROJECT_MANAGEMENT_PROJECT_TEMPLATE_KEY = "com.atlassian.jira-core-project-templates:jira-core-project-management";
	private final ProjectClient restClient;

	GeneratorControl(JIRAEnvironmentData environmentData) {
		super(environmentData);
		this.restClient = new ProjectClient(environmentData);
	}

	public Project generateScrumProject(String name) {
		return generateProject(SCRUM_SOFTWARE_PROJECT_TEMPLATE_KEY, name);
	}

	public Project generateKanBanProject(String name) {
		return generateProject(KANBAN_SOFTWARE_PROJECT_TEMPLATE_KEY, name);
	}

	public Project generateBasicProject(String name) {
		return generateProject(BASIC_SOFTWARE_PROJECT_TEMPLATE_KEY, name);
	}

	public Project generateBusinessProject(String name) {
		return generateProject(PROJECT_MANAGEMENT_PROJECT_TEMPLATE_KEY, name);
	}

	public Project generateProject(String templateKey, String name) {
		String projectName = capitalize(name);
		String projectKey = getProjectKeyForName(projectName);
		restClient.getProjects().stream()
		          .filter(p -> p.key.equals(projectKey))
		          .findFirst()
		          .ifPresent(project -> restClient.delete(project.key));
		JsonObject json = new JsonObject();
		json.addProperty("key", projectKey);
		json.addProperty("name", projectName);
		json.addProperty("lead", "admin");
		json.addProperty("projectTemplateKey", templateKey);
		ClientResponse response = restClient.create(json.toString());
		if (response.getStatusInfo() == ClientResponse.Status.CREATED) {
			return response.getEntity(Project.class).name(projectName).projectTypeKey(templateKey);
		} else {
			throw new IllegalStateException("Failed to create project " + name);
		}
	}

	private String getProjectKeyForName(String projectName) {
		String projectKey = projectName.replaceAll("[^A-Z]", "");
		if (projectKey.length() > 10) {
			projectKey = projectKey.substring(0, 10);
		}
		return projectKey;
	}
}
