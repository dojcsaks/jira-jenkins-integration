/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.sun.jersey.api.client.WebResource;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
class BackdoorControl<T extends BackdoorControl<T>> extends com.atlassian.jira.testkit.client.BackdoorControl<T> {

	BackdoorControl(JIRAEnvironmentData environmentData) {
		super(environmentData);
	}

	@Override
	protected String getRestModulePath() {
		return "jenkins-test";
	}

	WebResource createConfigurationResource() {
		return createResource().path("configuration");
	}

	WebResource createSiteResource() {
		return createResource().path("site");
	}

	WebResource createJobResource() {
		return createResource().path("job");
	}

	WebResource createBuildResource() {
		return createResource().path("build");
	}
}
