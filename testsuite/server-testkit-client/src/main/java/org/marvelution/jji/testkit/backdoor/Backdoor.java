/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.backdoor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class Backdoor extends com.atlassian.jira.testkit.client.Backdoor {

	private final JIRAEnvironmentData environmentData;
	private final ConfigurationControl configurationControl;
	private final SiteControl siteControl;
	private final JobControl jobControl;
	private final BuildControl buildControl;
	private final CacheControl cacheControl;
	private final ExtendedLicenseControl extendedLicenseControl;
	private final RawControl rawControl;
	private final GeneratorControl generatorControl;
	private final Map<Class<? extends RestApiClient>, RestApiClient> clients = new ConcurrentHashMap<>();
	private final Function<Class<? extends RestApiClient>, RestApiClient> clientCreator = type -> {
		try {
			return type.getConstructor(JIRAEnvironmentData.class).newInstance(environmentData());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	};

	public Backdoor() {
		this(new BackdoorEnvironmentData());
	}

	public Backdoor(JIRAEnvironmentData environmentData) {
		super(environmentData);
		this.environmentData = environmentData;
		configurationControl = new ConfigurationControl(environmentData);
		siteControl = new SiteControl(environmentData);
		jobControl = new JobControl(environmentData, siteControl);
		buildControl = new BuildControl(environmentData, jobControl);
		cacheControl = new CacheControl(environmentData);
		extendedLicenseControl = new ExtendedLicenseControl(environmentData);
		rawControl = new RawControl(environmentData);
		generatorControl = new GeneratorControl(environmentData);
	}

	public JIRAEnvironmentData environmentData() {
		return environmentData;
	}

	public ConfigurationControl configuration() {
		return configurationControl;
	}

	public SiteControl sites() {
		return siteControl;
	}

	public JobControl jobs() {
		return jobControl;
	}

	public BuildControl builds() {
		return buildControl;
	}

	public CacheControl cache() {
		return cacheControl;
	}

	@Override
	public ExtendedLicenseControl license() {
		return extendedLicenseControl;
	}

	@Override
	public RawControl rawRestApiControl() {
		return rawControl;
	}

	public GeneratorControl generator() {
		return generatorControl;
	}

	public <RC extends RestApiClient> RC createRestClient(Class<RC> type) {
		return type.cast(clients.computeIfAbsent(type, clientCreator));
	}
}
