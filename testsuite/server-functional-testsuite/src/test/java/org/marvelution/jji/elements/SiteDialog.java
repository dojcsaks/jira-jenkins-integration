/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.elements;

import com.codeborne.selenide.SelenideElement;

/**
 * @author Mark Rekveld
 * @since 24.0
 */
public class SiteDialog<S extends SiteDialog<S>> extends Dialog<S> {

	SiteDialog(SelenideElement opener) {
		super(opener, "#site-dialog");
	}

	public SelenideElement type() {
		return child("#type");
	}

	public SelenideElement name() {
		return child("#name");
	}

	public SelenideElement rpcUrl() {
		return child("#rpcUrl");
	}

	public SelenideElement displayUrl() {
		return child("#displayUrl");
	}

	public SelenideElement autoLinkNewJobs() {
		return child("#autoLinkNewJobs");
	}

	public SelenideElement user() {
		return child("#user");
	}

	public SelenideElement token() {
		return child("#token");
	}
}
