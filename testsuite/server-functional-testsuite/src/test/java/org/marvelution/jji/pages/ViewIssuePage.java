/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

/**
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class ViewIssuePage {

	private static final String URL_PREFIX = "/browse/";

	public static String url(String issueKey) {
		return URL_PREFIX + issueKey;
	}

	public static SelenideElement view() {
		return $(".issue-view");
	}

	public static SelenideElement header() {
		return view().find(".issue-header-content");
	}

	public static SelenideElement actionsContainer() {
		return view().find("#issue_actions_container");
	}

	public static SelenideElement summary() {
		return header().find("#summary-val");
	}

	public static SelenideElement issueKey() {
		return header().find(".issue-link");
	}

}
