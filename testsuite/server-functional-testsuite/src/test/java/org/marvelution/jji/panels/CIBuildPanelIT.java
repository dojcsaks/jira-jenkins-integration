/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.panels;

import org.marvelution.jji.AbstractFunctionalTest;
import org.marvelution.jji.elements.CIBuildsPanel;
import org.marvelution.jji.pages.ViewIssuePage;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Project;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;

/**
 * Functional test for the CI Builds issue tab panel
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class CIBuildPanelIT extends AbstractFunctionalTest {

	@Before
	public void setup() throws Exception {
		loginAsAdmin();
	}

	@After
	public void tearDown() throws Exception {
		logout();
	}

	@Test
	public void testCIBuildTabVisibleForScrumSoftwareProject() throws Exception {
		testCIBuildTabVisibilityForProject(backdoor.generator().generateScrumProject(testName.getMethodName()), true);
	}

	@Test
	public void testCIBuildTabVisibleForKanbanSoftwareProject() throws Exception {
		testCIBuildTabVisibilityForProject(backdoor.generator().generateKanBanProject(testName.getMethodName()), true);
	}

	@Test
	public void testCIBuildTabVisibleForBasicSoftwareProject() throws Exception {
		testCIBuildTabVisibilityForProject(backdoor.generator().generateBasicProject(testName.getMethodName()), true);
	}

	@Test
	public void testCIBuildTabNotVisibleForBusinessProject() throws Exception {
		testCIBuildTabVisibilityForProject(backdoor.generator().generateBusinessProject(testName.getMethodName()), false);
	}

	private void testCIBuildTabVisibilityForProject(Project project, boolean expectedVisibility) {
		IssueCreateResponse issue = backdoor.issues().createIssue(project.key, testName.getMethodName());
		refreshOrOpen(ViewIssuePage.url(issue.key));
		ViewIssuePage.issueKey().shouldBe(visible);
		ViewIssuePage.issueKey().shouldHave(text(issue.key));
		ViewIssuePage.summary().shouldBe(visible);
		ViewIssuePage.summary().shouldHave(text(testName.getMethodName()));
		if (expectedVisibility) {
			CIBuildsPanel.link().shouldBe(visible);
			CIBuildsPanel.link().click();
			CIBuildsPanel.message().shouldBe(visible);
			CIBuildsPanel.message().shouldHave(text("No builds found."));
		} else {
			CIBuildsPanel.link().shouldNotBe(visible);
		}
	}

}
