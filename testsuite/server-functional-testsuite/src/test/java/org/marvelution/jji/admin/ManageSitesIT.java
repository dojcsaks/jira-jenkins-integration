/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.admin;

import org.marvelution.jji.elements.AddSiteDialog;
import org.marvelution.jji.elements.ClearDeletedJobsDialog;
import org.marvelution.jji.elements.DeleteSiteDialog;
import org.marvelution.jji.elements.EditSiteDialog;
import org.marvelution.jji.elements.JenkinsJob;
import org.marvelution.jji.elements.JenkinsSite;
import org.marvelution.jji.elements.JenkinsSiteActions;
import org.marvelution.jji.elements.MessageBar;
import org.marvelution.jji.model.Job;
import org.marvelution.jji.model.Site;
import org.marvelution.jji.model.SiteType;
import org.marvelution.jji.pages.JenkinsConfigurationPage;
import org.marvelution.testing.wiremock.WireMockRule;

import org.junit.Rule;
import org.junit.Test;

import static org.marvelution.jji.conditions.Conditions.jobs;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.checked;
import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Condition.empty;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.exactValue;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;

/**
 * Functional tests for managing sites on the Jenkins Configuration page.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class ManageSitesIT extends AbstractAdminFunctionalTest {

	@Rule
	public WireMockRule jenkins = new WireMockRule().usingFilesUnderClass(ManageSitesIT.class);

	@Test
	public void testAddSite() throws Exception {
		loginAsAdminAndNavigateToJenkinsConfiguration();

		AddSiteDialog dialog = JenkinsConfigurationPage.addSite().open();

		// Assert validation errors on empty submit
		dialog.submit();
		dialog.fieldError(dialog.name()).shouldBe(visible).shouldHave(text("Name is required."));
		dialog.fieldError(dialog.rpcUrl()).shouldBe(visible).shouldHave(text("RPC URL is required."));

		// Assert validation errors on invalid URLs
		dialog.name().val(testName.getMethodName());
		dialog.rpcUrl().val("localhost");
		dialog.displayUrl().val("localhost");

		dialog.submit();
		dialog.fieldError(dialog.name()).shouldNotBe(visible);
		dialog.fieldError(dialog.rpcUrl()).shouldBe(visible).shouldHave(text("Invalid URL, missing host."));
		dialog.fieldError(dialog.displayUrl()).shouldBe(visible).shouldHave(text("Invalid URL, missing host."));

		dialog.rpcUrl().val(jenkins.serverUri().toASCIIString());
		dialog.displayUrl().val(jenkins.serverUri().toASCIIString());

		// Assert validation errors on missing auth details
		//submit without a token
		dialog.user().val("admin");
		dialog.submit()
		      .fieldError(dialog.token()).shouldBe(visible).shouldHave(text("Token is required when specifying an user."));
		//submit without an user
		dialog.user().val("");
		dialog.token().val("admin");
		dialog.submit()
		      .fieldError(dialog.user()).shouldBe(visible).shouldHave(text("User is required when specifying a token."));

		// Assert successful submission without auth details hides the dialog and adds the site
		dialog.user().val("");
		dialog.token().val("");
		dialog.submit();

		waitForSynchronizationToComplete();

		JenkinsSite site = JenkinsConfigurationPage.sites().site(testName.getMethodName()).shouldBe(visible);
		site.name().shouldHave(text(testName.getMethodName()));
		site.link().shouldHave(attribute("href", jenkins.serverUri().toASCIIString()));
		site.logo().shouldHave(cssClass("JENKINS-logo"));

		site.jobs().shouldHave(size(3), jobs("jira-jenkins-integration", "jira-jenkins-integration-common", "jenkins-jira-integration"));

		JenkinsJob job = site.jobs().job("jira-jenkins-integration");
		job.name().shouldHave(text("jira-jenkins-integration"));
		job.lastBuild().shouldHave(text("0"));
		job.linked().shouldNotBe(checked);

		job = site.jobs().job("jira-jenkins-integration-common");
		job.name().shouldHave(text("jira-jenkins-integration-common"));
		job.lastBuild().shouldHave(text("0"));
		job.linked().shouldNotBe(checked);

		job = site.jobs().job("jenkins-jira-integration");
		job.name().shouldHave(text("jenkins-jira-integration"));
		job.lastBuild().shouldHave(text("0"));
		job.linked().shouldNotBe(checked);
	}

	@Test
	public void testEditSite() throws Exception {
		backdoor.sites().addSite(SiteType.HUDSON, testName.getMethodName(), jenkins.serverUri());

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite site = JenkinsConfigurationPage.sites().site(testName.getMethodName()).shouldBe(visible);
		site.name().shouldHave(text(testName.getMethodName()));
		site.link().shouldHave(attribute("href", jenkins.serverUri().toASCIIString()));
		site.logo().shouldHave(cssClass("HUDSON-logo"));
		site.jobs().shouldHaveSize(0);

		JenkinsSiteActions actions = site.actions().open();
		actions.autolinkSite().shouldNotHave(cssClass("checked"));

		EditSiteDialog dialog = actions.editSite().open();
		dialog.type().shouldHave(exactValue("HUDSON"));
		dialog.name().shouldHave(exactValue(testName.getMethodName()));
		dialog.rpcUrl().shouldHave(exactValue(jenkins.serverUri().toASCIIString()));
		dialog.displayUrl().shouldBe(empty);
		dialog.autoLinkNewJobs().shouldNotBe(checked);
		dialog.user().shouldBe(empty);
		dialog.changeToken().shouldNotBe(checked);
		dialog.token().shouldNotBe(visible);
		dialog.close();

		dialog = actions.open().editSite().open();
		dialog.type().shouldHave(exactValue("HUDSON"));
		dialog.name().shouldHave(exactValue(testName.getMethodName()));
		dialog.rpcUrl().shouldHave(exactValue(jenkins.serverUri().toASCIIString()));
		dialog.displayUrl().shouldBe(empty);
		dialog.autoLinkNewJobs().shouldNotBe(checked);
		dialog.user().shouldBe(empty);
		dialog.changeToken().shouldNotBe(checked);
		dialog.token().shouldNotBe(visible);

		dialog.type().selectOptionByValue("JENKINS");
		dialog.displayUrl().val("localhost");
		dialog.autoLinkNewJobs().click();
		dialog.user().val("admin");
		dialog.changeToken().click();
		dialog.token().shouldBe(visible).val("admin");

		dialog.submit();
		dialog.fieldError(dialog.displayUrl()).shouldBe(visible).shouldHave(text("Invalid URL, missing host."));

		dialog.displayUrl().val("");
		dialog.submit();

		waitForSynchronizationToComplete();

		site.name().shouldHave(text(testName.getMethodName()));
		site.link().shouldHave(attribute("href", jenkins.serverUri().toASCIIString()));
		site.logo().shouldHave(cssClass("JENKINS-logo"));
		site.jobs().shouldHave(size(3), jobs("Jenkins Integration for JIRA Server", "Jenkins Integration for JIRA Commons",
		                                     "JIRA Integration for Jenkins"));

		JenkinsJob job = site.jobs().job("Jenkins Integration for JIRA Server");
		job.name().shouldHave(text("Jenkins Integration for JIRA"));
		job.lastBuild().shouldHave(text("0"));
		job.linked().shouldBe(checked);

		job = site.jobs().job("Jenkins Integration for JIRA Commons");
		job.name().shouldHave(text("Jenkins Integration for JIRA Commons"));
		job.lastBuild().shouldHave(text("0"));
		job.linked().shouldBe(checked);

		job = site.jobs().job("JIRA Integration for Jenkins");
		job.name().shouldHave(text("JIRA Integration for Jenkins"));
		job.lastBuild().shouldHave(text("0"));
		job.linked().shouldBe(checked);

		actions = site.actions().open();
		actions.autolinkSite().shouldHave(cssClass("checked"));
		
		dialog = actions.editSite().open();
		dialog.type().shouldHave(exactValue("JENKINS"));
		dialog.name().shouldHave(exactValue(testName.getMethodName()));
		dialog.rpcUrl().shouldHave(exactValue(jenkins.serverUri().toASCIIString()));
		dialog.displayUrl().shouldBe(empty);
		dialog.autoLinkNewJobs().shouldBe(checked);
		dialog.user().shouldHave(exactValue("admin"));
		dialog.token().shouldNotBe(visible);
		dialog.changeToken().shouldNotBe(checked).click();
		dialog.token().shouldBe(visible, empty);
		dialog.close();
	}

	@Test
	public void testDeleteSite() throws Exception {
		backdoor.sites().addSite(SiteType.HUDSON, testName.getMethodName(), jenkins.serverUri());

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite site = JenkinsConfigurationPage.sites().site(testName.getMethodName()).shouldBe(visible);
		site.name().shouldHave(text(testName.getMethodName()));
		site.link().shouldHave(attribute("href", jenkins.serverUri().toASCIIString()));
		site.logo().shouldHave(cssClass("HUDSON-logo"));

		DeleteSiteDialog dialog = site.actions().open().deleteSite().open();
		dialog.header().shouldHave(text("Delete Site:"), text(testName.getMethodName()));
		dialog.message().shouldHave(text("This action is not reversible."));
		dialog.close();

		JenkinsConfigurationPage.sites().shouldHaveSite(testName.getMethodName());

		site.actions().open().deleteSite().open().submit();

		JenkinsConfigurationPage.sites().shouldNotHaveSite(testName.getMethodName());

		refreshOrOpen(JenkinsConfigurationPage.URL);

		JenkinsConfigurationPage.sites().shouldNotHaveSite(testName.getMethodName());
	}

	@Test
	public void testEnableAutoEnableNewJobs() throws Exception {
		backdoor.sites().addSite(SiteType.JENKINS, testName.getMethodName(), jenkins.serverUri());

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite site = JenkinsConfigurationPage.sites().site(testName.getMethodName()).shouldBe(visible);
		site.name().shouldHave(text(testName.getMethodName()));
		site.link().shouldHave(attribute("href", jenkins.serverUri().toASCIIString()));
		site.logo().shouldHave(cssClass("JENKINS-logo"));
		site.jobs().shouldHaveSize(0);

		site.actions().open().autolinkSite().shouldNotHave(cssClass("checked")).click();

		JenkinsSiteActions actions = site.actions().open();
		actions.autolinkSite().shouldHave(cssClass("checked"));
		actions.refreshJobList().click();

		waitForSynchronizationToComplete();

		site.jobs().shouldHave(size(3), jobs("Jenkins Integration for JIRA Server", "Jenkins Integration for JIRA Commons",
		                                     "JIRA Integration for Jenkins"));

		JenkinsJob job = site.jobs().job("Jenkins Integration for JIRA Server");
		job.name().shouldHave(text("Jenkins Integration for JIRA"));
		job.lastBuild().shouldHave(text("0"));
		job.linked().shouldBe(checked);

		job = site.jobs().job("Jenkins Integration for JIRA Commons");
		job.name().shouldHave(text("Jenkins Integration for JIRA Commons"));
		job.lastBuild().shouldHave(text("0"));
		job.linked().shouldBe(checked);

		job = site.jobs().job("JIRA Integration for Jenkins");
		job.name().shouldHave(text("JIRA Integration for Jenkins"));
		job.lastBuild().shouldHave(text("0"));
		job.linked().shouldBe(checked);
	}

	@Test
	public void testClearDeletedJobs() throws Exception {
		Site site = backdoor.sites().addSite(SiteType.JENKINS, testName.getMethodName(), jenkins.serverUri());
		Job availableJob = backdoor.jobs().saveJob(new Job().setSite(site)
		                                                    .setName("available-job")
		                                                    .setDisplayName("Available Job")
		                                                    .setLinked(true));
		Job deletedJob = backdoor.jobs().saveJob(new Job().setSite(site)
		                                                  .setName("deleted-job")
		                                                  .setDisplayName("Deleted Job")
		                                                  .setLinked(true)
		                                                  .setDeleted(true));

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite jenkinsSite = JenkinsConfigurationPage.sites().site(testName.getMethodName()).shouldBe(visible);
		jenkinsSite.name().shouldHave(text(testName.getMethodName()));
		jenkinsSite.link().shouldHave(attribute("href", jenkins.serverUri().toASCIIString()));
		jenkinsSite.logo().shouldHave(cssClass("JENKINS-logo"));
		jenkinsSite.jobs().shouldHave(size(2), jobs(availableJob.getDisplayName(), deletedJob.getDisplayName()));
		jenkinsSite.jobs().job(availableJob.getDisplayName()).shouldNotHave(cssClass("job-deleted"));
		jenkinsSite.jobs().job(deletedJob.getDisplayName()).shouldHave(cssClass("job-deleted"));

		ClearDeletedJobsDialog dialog = jenkinsSite.actions().open().clearDeletedJobs().open();
		dialog.header().shouldHave(text("Clear Deleted Jobs from Site:"), text(testName.getMethodName()));
		dialog.message().shouldHave(text("This action is not reversible."));
		dialog.submit();

		jenkinsSite.jobs()
		           .shouldHaveJob(availableJob.getDisplayName())
		           .shouldNotHaveJob(deletedJob.getDisplayName())
		           .shouldNotHaveJob(deletedJob.getName());

		refreshOrOpenJenkinsConfiguration();

		jenkinsSite.jobs()
		           .shouldHaveJob(availableJob.getDisplayName())
		           .shouldNotHaveJob(deletedJob.getDisplayName())
		           .shouldNotHaveJob(deletedJob.getName());
	}

	@Test
	public void testSiteStatus_Online() throws Exception {
		backdoor.sites().addSite(SiteType.JENKINS, testName.getMethodName(), jenkins.serverUri());

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite site = JenkinsConfigurationPage.sites().site(testName.getMethodName());
		site.messageBar().messages().shouldHaveSize(0);
	}

	@Test
	public void testSiteStatus_PluginMissing() throws Exception {
		jenkins.expect(get(urlPathEqualTo("/plugin/jenkins-jira-plugin/ping.html"))
				               .atPriority(1).willReturn(aResponse().withStatus(404)));
		backdoor.sites().addSite(SiteType.JENKINS, testName.getMethodName(), jenkins.serverUri());

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite site = JenkinsConfigurationPage.sites().site(testName.getMethodName());
		MessageBar.Message message = site.messageBar().hints().shouldHaveSize(1).first();
		message.title().shouldHave(text("The plugin JIRA Plugin for Jenkins doesn't seem to be installed on"),
		                           text(testName.getMethodName()));
		message.message().shouldHave(
				text("Consider installing it to optimize the integration with JIRA and allow the use of Jenkins side"));
	}

	@Test
	public void testSiteStatus_NotAccessible() throws Exception {
		jenkins.expect(get(urlPathEqualTo("/api/json/")).atPriority(1).willReturn(aResponse().withStatus(401)));
		backdoor.sites().addSite(SiteType.JENKINS, testName.getMethodName(), jenkins.serverUri());

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite site = JenkinsConfigurationPage.sites().site(testName.getMethodName());
		MessageBar.Message message = site.messageBar().errors().shouldHaveSize(1).first();
		message.title().shouldHave(exactText("Not Accessible"));
		message.message().shouldHave(text("An error occurred connecting with"), text(testName.getMethodName()));
	}

	@Test
	public void testSiteStatus_Offline() throws Exception {
		backdoor.sites().addSite(SiteType.JENKINS, testName.getMethodName(), jenkins.serverUri());
		jenkins.client().shutdown();

		loginAsAdminAndNavigateToJenkinsConfiguration();

		JenkinsSite site = JenkinsConfigurationPage.sites().site(testName.getMethodName());
		MessageBar.Message message = site.messageBar().warnings().shouldHaveSize(1).first();
		message.message().shouldHave(text("seems to be offline"), text(testName.getMethodName()));
	}

	private void waitForSynchronizationToComplete() {
		waitForSynchronizationToComplete(() -> JenkinsConfigurationPage.sites().site(testName.getMethodName()).jobs().shouldHaveSize(3));
	}
}
