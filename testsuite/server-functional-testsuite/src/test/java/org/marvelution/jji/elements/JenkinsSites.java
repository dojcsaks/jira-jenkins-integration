/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.elements;

import org.marvelution.jji.conditions.Conditions;
import org.marvelution.jji.pages.JenkinsConfigurationPage;

import com.codeborne.selenide.ElementsCollection;

import static com.codeborne.selenide.Condition.attribute;

/**
 * Element that describes the Jenkins Site list on the {@link JenkinsConfigurationPage}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class JenkinsSites {

	private final ElementsCollection siteContainers;

	public JenkinsSites(ElementsCollection siteContainers) {
		this.siteContainers = siteContainers;
	}

	public JenkinsSites shouldHaveSite(String name) {
		siteContainers.filter(Conditions.site(name)).shouldHaveSize(1);
		return this;
	}

	public JenkinsSites shouldNotHaveSite(String name) {
		siteContainers.filter(Conditions.site(name)).shouldHaveSize(0);
		return this;
	}

	public JenkinsSite site(String name) {
		return new JenkinsSite(siteContainers.find(Conditions.site(name)).shouldHave(attribute("jenkins-site-id")));
	}
}
