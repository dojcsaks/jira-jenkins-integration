/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.conditions;

import java.util.List;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;

/**
 * Helper for custom {@link Condition} and {@link CollectionCondition} implementations.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class Conditions {

	public static Site site(String name) {
		return new Site(name);
	}

	public static Jobs jobs(String... expectedJobs) {
		return new Jobs(expectedJobs);
	}

	public static Jobs jobs(List<String> expectedJobs) {
		return new Jobs(expectedJobs);
	}

	public static Job job(String name) {
		return new Job(name);
	}
}
