/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.admin;

import java.util.concurrent.TimeUnit;

import org.marvelution.jji.AbstractFunctionalTest;
import org.marvelution.jji.elements.Flags;
import org.marvelution.jji.elements.SystemAdminMenu;
import org.marvelution.jji.pages.JenkinsConfigurationPage;

import org.awaitility.core.ThrowingRunnable;
import org.junit.After;

import static org.awaitility.Awaitility.with;

/**
 * Base functional tests for the {@link JenkinsConfigurationPage}.
 *
 * @author Mark Rekveld
 * @since 3.0.0
 */
public class AbstractAdminFunctionalTest extends AbstractFunctionalTest {

	@After
	@Override
	public void logout() {
		super.logout();
	}

	void loginAsAdminAndNavigateToJenkinsConfiguration() throws Exception {
		loginAsAdmin();
		new SystemAdminMenu().open().applications().click();
		JenkinsConfigurationPage.link().click();
		Flags.hideAllFlags();
	}

	void refreshOrOpenJenkinsConfiguration() {
		refreshOrOpen(JenkinsConfigurationPage.URL);
		Flags.hideAllFlags();
	}

	void waitForSynchronizationToComplete(ThrowingRunnable assertion) {
		// Wait for the synchronization to complete and the UI to be updated
		long delay = Long.parseLong(System.getProperty("jenkins.site.refresh.interval"));
		with().atMost(delay * 4, TimeUnit.MILLISECONDS).pollDelay(delay, TimeUnit.MILLISECONDS).pollInterval(1, TimeUnit.SECONDS)
		      .pollInSameThread().await().untilAsserted(assertion);
	}
}
